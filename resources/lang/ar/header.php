<?php
/**
 * Created by PhpStorm.
 * User: hanantaha
 * Date: 23/04/18
 * Time: 11:35 ص
 */
return [
    'about_us' => ' من نحن ',
    'login' => 'تسجيل الدخول',
    'register'  => 'حساب جديد',
    'entries'  => 'المشاركات',
    'ranks' => ' ترتيب المشاركين ',
    'books' => 'الكتب',
    'suggest_book'  => ' أقترح كتاب',
    'old_work'  => ' الكتب المحفوظة',
    'header_title' => 'الكتب',
    'logout'       => 'تسجيل خروج',
    'about_contest' => 'عن المسابقة',
    'rating_item'   => ' بنود التقييم'

];
<?php

return [



    'arabic' => 'عربى',
    'english' => 'إنجليزى',
    'site_name'  => 'الكتب',
    'about_us'  => ' من نحن',
    'login' => [
        'title'   => 'تسجيل الدخول',
        'email'   => 'البريد الإلكترونى',
        'password' => 'كلمة المرور',
        'submit'  => 'دخول',
    ],
    'register' => [
        'title'   => 'تسجيل حساب جديد',
        'name'    => 'اﻷسم كاملا ',
        'email'   => 'البريد الإلكترونى',
        'password' => 'كلمة المرور',
        'password_confirm' => 'تأكيد كلمة المرور ',
        'nationality'   => 'الجنسية',
        'speciality'   => 'التخصص',
        'phone' => 'الهاتف',
        'gender'  => 'النوع',
        'male'   => 'ذكر',
        'female'  => 'أنثى',
        'submit'  => 'تسجيل',
    ],
    'entries'  => [
        'page_title'   => 'المشاركات',
        'no_data_found'      => 'لا توجد بيانات للعرض',
        'client_name'  => 'إسم ',
        'book_name'    => 'إسم الكتاب ',
        'date'         => 'التاريخ',
        'rate'         => 'التقيم',
        'summery'      => 'الملخص',
        'critisim'     => 'النقد',
        'save_draft'   => 'حفظ كمسودة',
        'submit'       => 'حفظ',
        'critisim_prectange'  => 'نسبة النقد'
    ],
    'ranks'  => [
    'page_title'   => 'ترتيب المشاركات',
    'no_data_found'      => ' لا توجد بيانات للعرض',
    'client_name'  => 'إسم العميل ',
    'total_rate'    => '  التقيم الكلى',
    'entry_count'         => 'عدد المشاركات ',
],
    'suggest_book'  => [
        'page_title'   => '  إقترح كتاب',
        'book_name'    => 'إسم كتاب ',
        'book_description'    => 'الوصف ',
        'author'         => 'المؤلف ',
        'submit'         => 'إرسال',
    ],
    'books'  => [
        'page_title'   => ' الكتب ',
        'no_data_found'    => ' لا توجد كتب ',
        'download'         => 'تحميل',
        'book_details'     => 'تفاصيل كتاب ',
        'add_entry'     => 'إضافة مشاركة  ',
    ],
    'old_work' => [
        'page_title'   => ' المسودات ',
        'book_desc'  => ' وصف الكتاب',
        'summary'    => 'الملخص',
        'critism'   =>'النقد',
        'edit_entry'   =>'تحديث مشاركة ',
        'no_data_found'  => ' لاتوجد مشاركات للعرض '

    ],
    'book_name'   => 'إسم الكتاب',
    'search'          => 'إبحث'


];
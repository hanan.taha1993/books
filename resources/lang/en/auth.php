<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Login',
    'email' => 'email',
    'password' => 'password',
    'remmber_me' => 'Remmber Me',
    'login_button' => 'Login',
    'login_sentence'  => 'Please Login',
    'forget_password'  => 'Forget Password',

];

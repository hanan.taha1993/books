<?php

return [



    'arabic' => 'Ar',
    'english' => 'En',
    'site_name'  => 'Books',
    'about_us'  => 'About US',
    'login' => [
        'title'   => 'LOGIN',
        'email'   => 'Email',
        'password' => 'Password',
        'submit'  => 'Submit',

    ],
    'register' => [
        'title'   => 'REGISTER',
        'name'    => 'Full Name',
        'email'   => 'Email',
        'password' => 'Password',
        'password_confirm' => ' Confirm Password',
        'nationality'   => 'Nationality',
        'speciality'   => 'Speciality',
        'phone' => 'Phone',
        'gender'  => 'gender',
        'male'   => 'Male',
        'female'  => 'Female',
        'submit'  => 'Submit',
    ],
    'entries'  => [
        'page_title'   => 'CONTENTS',
        'no_data_found'      => ' NO CONTEST TO SHOW',
        'client_name'  => 'Name',
        'book_name'    => 'Book Name',
        'date'         => 'Date',
        'rate'         => 'Rate',
        'summery'      => 'ٍsummery',
        'critisim'     => 'Criticism',
        'save_draft'   => 'Save Draft',
        'submit'       => 'Submit',
        'critisim_prectange'  => 'critisim Prectange'
    ],
     'ranks'  => [
    'page_title'   => 'User Rank',
    'no_data_found'      => ' NO Data TO SHOW',
    'client_name'  => 'Client Name',
    'total_rate'    => ' Total Rate',
    'entry_count'         => 'Entry Count',
],
    'suggest_book'  => [
        'page_title'   => 'Suggest Book ',
        'book_name'    => 'Book Name',
        'book_description'    => 'Description ',
        'author'         => 'Author ',
        'submit'         => 'Submit',
    ],

    'books'  => [
        'page_title'   => ' Books ',
        'no_data_found'    => 'NO BOOKS TO SHOW ',
        'download'         => 'Download',
        'book_details'     => 'Book Details',
        'add_entry'     => 'Add Entry ',
    ],

    'old_work' => [
        'page_title'   => ' Old Works ',
        'book_desc'  => 'Book description',
        'summary'    => 'Summary',
        'critism'   =>'Critism',
        'edit_entry'   =>'Edit Entry',
        'no_data_found'  => 'No Old Works To Show '

    ],
     'book_name'   => 'Book Name ',
    'search'          => 'Search'


];
<?php
/**
 * Created by PhpStorm.
 * User: hanantaha
 * Date: 23/04/18
 * Time: 11:35 ص
 */
return [
    'about_us' => 'About us',
    'login' => 'Login',
    'register'  => 'Register',
    'entries'  => 'Entries',
    'ranks' => ' Ranks ',
    'books' => 'Books',
    'suggest_book'  => 'Suggest Book',
    'old_work'  => 'Old Work',
    'header_title' => 'Books',
    'logout'       => 'Logout',
    'about_contest' => 'About Contest',
    'rating_item'   => 'Rating items'
];
<?php

return [

 'control_panel'   => 'Control Panel',
 'home'            => 'Home',
 'users'           => 'Users',
 'admin'           => [

 	'change_password'       => 'Change Password',
 	'current_password'      => 'Current Password',
 	'new_password'          => 'New Password',
 	'confirm_new_password'  => 'Confirm New Password',
 	'email'                 => 'Email'
 ],

 'footer'          => [
 
 	'right_reserved'     => 'All Right Reserved',
 	'company_name'       => 'Ipda3',
 ],

 'save'                  => 'Save',
 'logout'                => 'Logout',
	'settings'             => [
		'name'               => 'Settings',
		'desc'               => 'website Settings',
	  'site_name'            => 'Site Name',
		'facebook'            => 'Facebook',
		'twitter'            => 'Twitter',
		'linked_in'            => 'Linked In',
		'share_limit'            => 'Share Limit Number',
		'about_us'            => 'About Us',
		'share_content'            => 'Share Content',
		'social_media_accounts'   => 'Social Media Accounts',
		'success'                 => 'Settings has been saved successfully',
	],

	'sliders'         => [
		'title'        => 'Sliders',
		'show_sliders'    => 'Show Sliders',
		'new_slider'    => 'New Slider',

	],
	'categories'         => [
		'title'        => 'categories',
		'show_sliders'    => 'Show Sliders',
		'new_slider'    => 'New Slider',

	],

];
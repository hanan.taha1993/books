@extends('front.layouts.main')
@section('content')
@inject('category','App\Models\Category')
    <div class="books">
        <div class="container">
            <div class="row">
                <h1 class="bo">{{trans('mainpage.books.page_title')}} </h1>
                   <div class="row">
            {!! Form::open([
                'method' => 'GET'
            ]) !!}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('name',\Request::input('name'),[
                        'class' => 'form-control',
                        'placeholder' => trans('mainpage.book_name')
                    ]) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::select('category_id',$category->pluck(session()->get('lang').'_name','id')->toArray(),\Request::input('category_id'),
                    [    'class' => 'form-control',
                    ]) !!}
                </div>
            </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button class="btn btn-flat btn-block btn-primary">{{trans('mainpage.search')}}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
                @if(count($books)>0)
                    <div class="clearfix"></div>
                    @foreach($books as $book)
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <a href="{{url('client/book/'.$book->id)}}">
                                <div class="book">
                                    <!-- @if(!empty($book->thumbnail))
                                        <img src="{{asset($book->thumbnail)}}">
                                    @endif -->
                                    <h1>{{ $book->{session()->get('lang').'_name'} }}</h1>
                                    <h1>{{ $book->author }}</h1>

                                    <!-- <p> {!! str_limit($book->{session()->get('lang').'_description'} ,300) !!}
                                        <span><a href="{{url('client/book/'.$book->id)}}">More</a></span>
                                    </p>
                                    @if(!empty($book->file))
                                        <a href="{{url($book->file)}}" class="btn dl_btn " target="_blank"><i
                                                    class="fa fa-download"></i> {{trans('mainpage.books.download')}}
                                        </a>
                                    @endif -->
                                </div>
                            </a>
                        </div>
                    @endforeach
                @else
                    <p> {{trans('mainpage.books.no_data_found')}}  </p>
                @endif
            </div>
        </div>
    </div>
    </div>
@stop
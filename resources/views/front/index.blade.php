@extends('front.layouts.main')
@section('content')

  <div class="slider">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        @foreach($sliders as $slider)
          @if($loop->first)
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            @else
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          @endif
        @endforeach
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        @foreach($sliders as $slider)
          @if($loop->first)
        <div class="item active">
          @else
            <div class="item ">
          @endif
          <img src="{{$slider->thumbnail}}" alt="">
          <div class="carousel-caption">
            {{ $slider->{session()->get('lang').'_title'} }}<br/>
            <span>{!! str_limit($slider->{session()->get('lang').'_description'},20) !!}</span>
          </div>
        </div>
          @endforeach
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  <div class="about" id="about_us">
    <div class="container">
      <div class="row">
        <h1> {{trans('mainpage.about_us')}}</h1>
        <p> {!! settings()->{session()->get('lang').'_about_us'} !!}  </p>
        <a href="{{ settings()->facebook ?? '#'}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a href="{{ settings()->twitter ?? '#'}}" target="_blank"><i class="fab fa-twitter"></i></a>
        <a href="{{settings()->linked_in ?? '#'}}" target="_blank"><i class="fab fa-linkedin-in"></i></a>
      </div>
    </div>
  </div>
  <!-- <div class="footer">
    All Right Reversed &copy; 2018
  </div> -->
  <div class="alert-box">
    <div class="alert-btn">
    <h1>Alert</h1>
      <i class="fas fa-times-circle"></i>
      <div class="container-fluid">
      @if(session()->has('success'))
      <div class="alert alert-success">
      	{{session()->get('success')}}
      </div>
      @endif
      
      @if(session()->has('error'))
      <div class="alert alert-danger">
      	{{session()->get('error')}}
      </div>
      @endif
      
      @include('admin.layouts.partials.validation-errors')
      </div>
    </div>
  </div>
  <div class="login">
    <div class="log">
      <h1>{{trans('mainpage.login.title')}}</h1>
      <i class="fas fa-times-circle"></i>
      <form method="POST" action="{{url('client_login')}} " role="form">
        {{csrf_field()}}
        <input type="text" placeholder="{{trans('mainpage.login.email')}}" name="email" required>
        <input type="password" placeholder="{{trans('mainpage.login.password')}}" name="password"  required>
        <input type="submit" name="" value="{{trans('mainpage.login.submit')}}" id="login">
      </form>
    </div>
  </div>
  <div class="register">
    <div class="reg">
      <h1>{{trans('mainpage.register.title')}}</h1>
      <i class="fas fa-times-circle"></i>
      <form action="{{url('client_register')}}" role="form" method="POST">
        {{csrf_field()}}
        <input type="text" placeholder="{{trans('mainpage.register.name')}} " name="name" required>
        <input type="text" placeholder="{{trans('mainpage.register.phone')}}" name="phone" required>
        <input type="email" name="email" placeholder="{{trans('mainpage.register.email')}}" required>
        <input type="text" placeholder="Nationality" name="{{trans('mainpage.register.nationality')}}" required>
        <input type="text" placeholder="Speciality" name="{{trans('mainpage.register.speciality')}}" required>
        <p>{{trans('mainpage.register.gender')}}

          <input type="radio" id="gender_male"
                 name="gender" value="male" checked="checked">
          <label for="gender_male">{{trans('mainpage.register.male')}}</label>
          <input type="radio" id="gender_female"
                 name="gender" value="female">
          <label for="gender_female">{{trans('mainpage.register.female')}}</label>
        </p>
        <input type="password" placeholder="{{trans('mainpage.register.password')}}" name="password" required>
        <input type="password" placeholder="{{trans('mainpage.register.password_confirm')}}" name="password_confirmation" required>
        <input type="submit" name="" value="Submit">
      </form>
    </div>
  </div>
@stop
@extends('front.layouts.main')
@section('content')
    <div class="books">
        <div class="container">
            <div class="row">
                <h1 class="bo">{{trans('mainpage.entries.page_title')}}</h1>
                @if(count($entries)>0)
                <div class="clearfix"></div>
                @foreach($entries as $entry)
                <div class="col-lg-3 col-md-4 col-xs-6">
                    <div class="entry">
                        <h1>Entry #{{$loop->iteration}}</h1>
                        <p><i class="fas fa-dot-circle"></i>{{trans('mainpage.entries.client_name')}} : {{ $entry->client->name ?? '' }}</p>
                        <p><i class="fas fa-book"></i>{{trans('mainpage.entries.book_name')}}  : {{ $entry->{session()->get('lang').'_name'} }}</p>
                        <p><i class="fas fa-calendar-alt"></i>{{trans('mainpage.entries.date')}} : {{$entry->created_at->format('m/d/Y')}} </p>

                        <div class="rating-nd-price">
                            <span><i class="fas fa-star"></i>{{trans('mainpage.entries.rate')}} : {{round($entry->rates()->avg('rate_value'),0)}}%</span>
                            <ul class="rating-stars">
                                <li class="review-start">
                                    <div class="rating_stars">
                                        <div class="rating_down">
                                            <div class="rating_up" style="width:{{$entry->rates()->avg('rate_value')}}%;"></div>
{{--                                            <div class="rating_up" style="{{'width:'.$entry->rates()->avg('rate_value') * 0.05 .'%'}}"></div>--}}
                                        </div>
                                    </div>
                                </li>
                            </ul></div>
                    </div>
                </div>
                    @endforeach
                    @else
                    <p> {{trans('mainpage.entries.no_data_found')}}   </p>
                @endif
            </div>
        </div>
    </div>
    </div>
    @stop
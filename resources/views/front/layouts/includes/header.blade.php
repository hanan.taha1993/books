<!DOCTYPE html>
<html lang="{{session()->get('lang')}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="view-port" content="width=device-width, intial-scale=0">
     <title>
                 @if(!empty(session()->get(session()->get('lang').'_site_name')))
                     {{ session()->get(session()->get('lang').'_site_name')  }}
                 @else
                     {{trans('mainpage.site_name')}}
                 @endif
               </title>
    <meta name="description" content="An interactive getting started guide for Brackets.">
      @if(session()->get('lang') == 'ar')
        <link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/bootstrap.css">
        <link  type="text/css"  href="{{asset('front')}}/css/bootstrap-rtl.min.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/style_rtl.css">
    @else
          <link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/bootstrap.css">

  
    @endif

    <link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/fontawesome-all.css">
              <link rel="stylesheet" type="text/css" href="{{asset('front')}}/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('front')}}/images/logo.png">BOOKS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        @if(Auth::guard('client')->check())
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                @if(session()->get('lang') == 'ar')
                <ul class="nav navbar-nav navbar-left">
                    @else
                        <ul class="nav navbar-nav navbar-right">
                     @endif
                        <li><a href="{{url('client/entries')}}">{{trans('header.entries')}}</a></li>
                        <li><a href="{{url(settings()->about_contest_file)}}" target="_blank"></i> {{trans('header.about_contest')}}</a></li>
                        <li><a href="{{url(settings()->rating_item_file)}}"  target="_blank">{{trans('header.rating_item')}}</a></li>

                    <li><a href="{{url('client/my_entry')}}">{{trans('header.ranks')}} </a></li>
                    <li><a href="{{url('client/books')}}">{{trans('header.books')}}</a></li>
                    <li><a href="{{url('client/suggest_book')}}">{{trans('header.suggest_book')}} </a></li>
                   <li><a href="{{url('client/old_work')}}">{{trans('header.old_work')}} </a></li>
                    @if(session()->has('lang'))
                    @if(session()->get('lang') == 'ar')
                      <li>
                        <a href="{{ url('lang/en') }}"><i class="fa fa-globe"></i><span>{{trans('mainpage.english')}}</span></a>
                      </li>
                      @else
                      <li>
                        <a href="{{ url('lang/ar') }}"><i class="fa fa-globe"></i><span>{{trans('mainpage.arabic')}}</span></a>
                      </li>
                    @endif
                    @else
                      <li>
                        <a href="{{ url('lang/ar') }}"><i class="fa fa-globe"></i><span>{{trans('mainpage.arabic')}}</span></a>
                      </li>
                    @endif 
                    <li>
                        <script type="">
                            function submitSignout(){
                                document.getElementById('signoutForm').submit();

                            }
                        </script>
                        {!! Form::open(['method' => 'post', 'url' => url('client/logout'),'id'=>'signoutForm']) !!}

                        {!! Form::close() !!}

                        <a href="#" onclick="submitSignout()">
                            <i class="fas fa-sign-out-alt"></i>{{trans('header.logout')}}
                        </a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
            @else
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{url('/#about_us')}}"> {{trans('header.about_us')}}</a></li>
                <li><a href="{{url('/#login_click')}}" id="login_click"><i class="fas fa-user-plus"></i>{{trans('header.login')}}</a></li>
                <li><a href="{{url('/#reg_click')}}" id="reg_click"><i class="fas fa-lock"></i>{{trans('header.register')}}</a></li>
                <li><a href="{{url('client/entries')}}">{{trans('header.entries')}}</a></li>
                 @if(session()->has('lang'))
                @if(session()->get('lang') == 'ar')
                  <li>
                    <a href="{{ url('lang/en') }}"><i class="fa fa-globe"></i><span>{{trans('mainpage.english')}}</span></a>
                  </li>
                  @else
                  <li>
                    <a href="{{ url('lang/ar') }}"><i class="fa fa-globe"></i><span>{{trans('mainpage.arabic')}}</span></a>
                  </li>
                @endif
                @else
                  <li>
                    <a href="{{ url('lang/ar') }}"><i class="fa fa-globe"></i><span>{{trans('mainpage.arabic')}}</span></a>
                  </li>
              @endif 

            </ul>
        </div><!-- /.navbar-collapse -->
            @endif
    </div><!-- /.container-fluid -->
</nav>
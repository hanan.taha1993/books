 <div class="footer">
     {{trans('footer.right_reserved')}} &copy; 2018
   </div>
 <script src="{{asset('front')}}/js/jquery.js"></script>
 <script src="{{asset('front')}}/js/bootstrap.min.js"></script>
 <script src="{{asset('front')}}/js/custom.js"></script>
@if(session()->has('success') OR  session()->has('error') OR $errors->any() ) 
<script>
	$('.alert-box').css({"display":"block"});
</script>
@endif
 </body>
 </html>
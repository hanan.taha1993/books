@extends('front.layouts.main')

@section('content')
    <div class="contents">
    <div class="container">
        <div class="row">
            <h1>{{trans('mainpage.books.book_details')}}  </h1>
            <div class="the_book col-xs-12">
                <img src="{{asset($book->thumbnail)}}">
                <h2>{{ $book->{session()->get('lang').'_name'} }}</h2>
                <p> {{ $book->{session()->get('lang').'_description'} }}  </p>
                @if( $entry==0)
                    <div class="clearfix"></div>
                    <hr>
                <h3> {{trans('mainpage.books.add_entry')}}</h3>
                    <form class="form-horizontal" autocomplete="off" enctype="multipart/form-data" method="POST" action="{{url('client/add_entry')}}">
                        {{csrf_field()}}
                        <div class="form-group form-group-lg">
                                <input type="hidden" value="{{$book->id}}" name="book_id">
                        </div>
                        <div class="form-group form-group-lg ">
                            <label class="col-sm-2 control-label">{{trans('mainpage.entries.summery')}}</label>
                            <div class="col-sm-10 col-md-9">
              <textarea name="summary" class="form-control live" required="required"
                        placeholder="{{trans('mainpage.entries.summery')}}" data-class="live-title"> </textarea>
                            </div>
                        </div>
                        <div class="form-group form-group-lg ">
                            <label class="col-sm-2 control-label">{{trans('mainpage.entries.critisim')}}</label>
                            <div class="col-sm-10 col-md-9">
              <textarea name="criticism" class="form-control live" required="required"
                        placeholder="{{trans('mainpage.entries.critisim')}}" data-class="live-title"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-group-lg ">
                            <label class="col-sm-2 control-label">{{trans('mainpage.entries.critisim_prectange')}}</label>
                            <div class="col-sm-10 col-md-9">
              <input type="number"  name="critisim_prectange" class="form-control live" required="required"
                        placeholder="{{trans('mainpage.entries.critisim_prectange')}}" data-class="live-title"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-group-lg">
                            <div class="col-sm-offset-2 col-sm-10  col-md-9">
                                <input type="submit" value="{{trans('mainpage.entries.submit')}}" class="btn btn-primary btn-lg+-" name="submit">
                                <input type="submit" value="{{trans('mainpage.entries.save_draft')}} " class="btn btn-primary btn-lg+-" name="submit">
                            </div>
                        </div>
                    </form>
                    @endif
            </div>
        </div>
    </div>
</div>
    @stop
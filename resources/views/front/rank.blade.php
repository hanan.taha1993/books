@extends('front.layouts.main')
@section('content')
    <div class="contents">
        <div class="container">
            <div class="row">
                <h1>{{trans('mainpage.ranks.page_title')}}  </h1>
                        <div class="my_work col-xs-12">
                    		<div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th class="text-center">#</th>
                <th class="text-center"> {{trans('mainpage.ranks.client_name')}}  </th>
                <th class="text-center" > {{trans('mainpage.ranks.total_rate')}}  </th>
                <th class="text-center"> {{trans('mainpage.ranks.entry_count')}}   </th>
                </thead>
                <tbody>
                    @php $count = 1; @endphp
                    @foreach($entries as $entry)
                        <tr>
                            <td>{{$count}}</td>
                            <td> {{optional($entry->client)->name}}</td>
                            <td> {{$entry->total }}</td>
                            <td> {{$entry->count }}</td>
                        </tr>
                        @php $count ++; @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
                        </div>
            </div>
        </div>
    </div>
@stop
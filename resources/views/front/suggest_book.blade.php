@extends('front.layouts.main')
@section('content')
    <div class="contents">
    <div class="container">
        <div class="row">
            @include('flash::message')
            <h1>{{trans('mainpage.suggest_book.page_title')}} </h1>
            <form class="form-horizontal" autocomplete="off" enctype="multipart/form-data" action="{{url('client/suggest_book')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group form-group-lg ">
                    <label class="col-sm-2 control-label">{{trans('mainpage.suggest_book.book_name')}} </label>
                    <div class="col-sm-10 col-md-9">
                        <input type="text" name="name" class="form-control live" required="required"
                               placeholder="{{trans('mainpage.suggest_book.book_name')}} " data-class="live-title">
                    </div>
                </div>
                <div class="form-group form-group-lg">
                    <label class="col-sm-2 control-label">{{trans('mainpage.suggest_book.book_description')}}</label>
                    <div class="col-sm-10 col-md-9">
                        <input type="text" name="description" class="form-control live" required="required" placeholder="{{trans('mainpage.suggest_book.book_description')}}" data-class="live-desc">
                    </div>
                </div>
                <div class="form-group form-group-lg">
                    <label class="col-sm-2 control-label">{{trans('mainpage.suggest_book.author')}}</label>
                    <div class="col-sm-10 col-md-9">
                        <input type="text" name="author" class="form-control live" required="required" placeholder="{{trans('mainpage.suggest_book.author')}}" data-class="live-desc">
                    </div>
                </div>
                <div class="form-group form-group-lg">
                    <div class="col-sm-offset-2 col-sm-10  col-md-9">
                        <input type="submit" value="{{trans('mainpage.suggest_book.submit')}}" class="btn btn-primary btn-lg+-">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    @stop
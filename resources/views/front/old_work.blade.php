@extends('front.layouts.main')
@section('content')
<div class="contents">
    <div class="container">
        <div class="row">
            <h1>{{trans('mainpage.old_work.page_title')}}</h1>
            @if(count($works)>0)
            @foreach($works as $work)
            <div class="my_work col-xs-12">
                <h2><i class="fas fa-dot-circle"></i>{{ $work->book->{session()->get('lang').'_name'} }}</h2>
                <hr style="border-color: #CCC;">
                <p><i class="fas fa-dot-circle"></i><strong>{{trans('mainpage.old_work.book_desc')}}</strong>
                    {!! $work->book->{session()->get('lang').'_description'} !!}
                </p>
                <hr style="border-color: #CCC;">
                <p><i class="fas fa-dot-circle"></i><strong> {{trans('mainpage.old_work.summary')}} </strong>
                    {{ $work->summary  }}
                </p>
                <hr style="border-color: #CCC;">
                <p><i class="fas fa-dot-circle"></i><strong> {{trans('mainpage.old_work.critism')}}</strong>
                        {{$work->criticism}}
                </p>
                <p> <a href="{{url('client/edit_draft/'.$work->id)}}" class="btn btn-info">  {{trans('mainpage.old_work.edit_entry')}} </a></p>
            </div>
                @endforeach
                @else
                <p> {{trans('mainpage.old_work.no_data_found')}}</p>
            @endif
        </div>
    </div>
</div>
    @stop
@extends('front.layouts.main')
@section('content')
<div class="contents newworks">
    <div class="container">
        <div class="row">
            @include('flash::message')
            <h1>New Work</h1>
            <form class="form-horizontal" autocomplete="off" enctype="multipart/form-data" method="POST" action="{{url('client/add_entry')}}">
                {{csrf_field()}}
                <div class="form-group form-group-lg">
                    <label class="col-sm-2 control-label">Book Name</label>
                    <div class="col-sm-10 col-md-9">
                        <select name="book_id" class="form-control" readonly="" required="">
                            <option value="{{$book->id}}">{{ $book->{session()->get('lang').'_name'} }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-lg ">
                    <label class="col-sm-2 control-label">Summary</label>
                    <div class="col-sm-10 col-md-9">
              <textarea name="summary" class="form-control live" required="required"
                        placeholder="Summary" data-class="live-title"> @if($entry) {{$entry->summary}}@endif</textarea>
                    </div>
                </div>
                <div class="form-group form-group-lg ">
                    <label class="col-sm-2 control-label">Criticism</label>
                    <div class="col-sm-10 col-md-9">
              <textarea name="criticism" class="form-control live" required="required"
                        placeholder="Criticism" data-class="live-title">@if($entry) {{$entry->criticism}}@endif</textarea>
                    </div>
                </div>
                @if($entry)
                <input type="hidden"  value="{{$entry->id}}" name="entry_id">
                @endif
                <div class="form-group form-group-lg">
                    <div class="col-sm-offset-2 col-sm-10  col-md-9">
                        <input type="submit" value="Submit" class="btn btn-primary btn-lg+-" name="submit">
                        <input type="submit" value="Save Draft" class="btn btn-primary btn-lg+-" name="submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    @stop
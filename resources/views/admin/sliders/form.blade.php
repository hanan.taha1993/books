@include('admin.layouts.partials.validation-errors')
@include('flash::message')
{!! \App\Ibnfarouk\MyClasses\Field::text('en_title',' English Title ') !!}
{!! \App\Ibnfarouk\MyClasses\Field::text('ar_title','Arabic Title ') !!}

{!! \App\Ibnfarouk\MyClasses\Field::fileWithPreview('thumbnail','Thumbnail ') !!}
@if($model->thumbnail)
    <div class="col-md-4">
        <img src="{{asset($model->thumbnail)}}" alt="" class="img-responsive thumbnail">
    </div>
@endif
<div class="clearfix"></div>
{!! \App\Ibnfarouk\MyClasses\Field::editor('en_description','English description ') !!}
{!! \App\Ibnfarouk\MyClasses\Field::editor('ar_description','Arabic description ') !!}


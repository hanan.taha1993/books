@extends('admin.layouts.main',[
								'page_header'		=> 'Slider',
								'page_description'	=> 'show thumbnails'
								])
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <div class="pull-left">
                <a href="{{url('admin/slider/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i>New Slider
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="box-body">
            @include('flash::message')
        @if(!empty($sliders) && count($sliders)>0)
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th> English Title</th>
                <th> Arabic Title</th>
                <th> thumbnail </th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
                </thead>
                <tbody>
                    @php $count = 1; @endphp
                    @foreach($sliders as $slider)
                        <tr id="removable{{$slider->id}}">
                            <td>{{$count}}</td>
                            <td> {{$slider->en_title}}</td>
                            <td> {{$slider->ar_title}}</td>
                            <td> <a href="{{asset($slider->thumbnail)}}" data-lightbox="image-1" data-title="{{$slider->title}}">
                                    <img src="{{asset($slider->thumbnail)}}" alt="" style="height: 70px;"></a> </td>
                            <td class="text-center"><a href="{{url('admin/slider/'.$slider->id. '/edit')}}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                             <td class="text-center">
                                <button id="{{$slider->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('slider.destroy',$slider->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                        @php $count ++; @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
            {!! $sliders->render() !!}
        @else
                <div>
                    <h3 class="text-info" style="text-align: center">No Data To Show </h3>
                </div>
            @endif
    </div>
</div>
@stop
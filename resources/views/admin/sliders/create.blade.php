@extends('admin.layouts.main',[
                                'page_header'       => ' Sliders',
                                'page_description'  => '  New thumbnail'
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'SliderController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files'=>true
                            ])!!}

    <div class="box-body">

        @include('admin.sliders.form')

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection
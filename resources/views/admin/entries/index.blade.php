@extends('admin.layouts.main',[
                                'page_header'       => ' Entries',
                                'page_description'  => '  all Entries'
                                ])
@inject('book','App\Models\Book')
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <div class="clearfix"> </div>
            <div class="row">
            {!! Form::open([
                'method' => 'GET'
            ]) !!}
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::select('state',$book->pluck('name','id')->toArray(),\Request::input('state'),
                    [    'class' => 'form-control',
                        'placeholder' => 'please select book'
                    ]) !!}
                </div>
            </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button class="btn btn-flat btn-block btn-primary">search</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="box-body">
            @include('flash::message')
        @if(!empty($entries) && count($entries)>0)
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th> client name</th>
                <th class="text-center">Delete</th>
                </thead>
                <tbody>
                    @php $count = 1; @endphp
                    @foreach($entries as $entry)
                        <tr id="removable{{$entry->id}}">
                            <td>{{$count}}</td>
                            <td> {{$entry->client->name}}</td>
                            <td> {{$entry->book->name}}</td>
                        </tr>
                        @php $count ++; @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
            {!! $entries->render() !!}
        @else
                <div>
                    <h3 class="text-info" style="text-align: center">No Data To Show </h3>
                </div>
            @endif
    </div>
</div>
@stop
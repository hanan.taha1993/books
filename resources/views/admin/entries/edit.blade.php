@extends('admin.layouts.main',[
                                'page_header'       => ' entries',
                                'page_description'  => ' Edit Entry'
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['EntryController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT',
                            'files'=>true
                            ])!!}

    <div class="box-body">

        @include('admin.entries.form')

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection
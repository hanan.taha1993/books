@extends('admin.layouts.main',[
								'page_header'		=> 'Entries',
								'page_description'	=> 'Entry  details'
								])
@section('content')
    <div class="box box-primary">
        <div class="box-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="data-table table table-bordered">
                    <tbody>
                    <tr>
                        <td> book name</td>
                        <td>{{optional($entry->book)->en_name ?? '-' }} - {{optional($entry->book)->ar_name ?? '-' }}</td>
                    </tr>

                    <tr>
                        <td> client name</td>
                        <td>{{optional($entry->client)->name ?? 'no name !!' }}</td>
                    </tr>
                    <tr>
                        <td> Entry summary</td>
                        <td>{!! $entry->summary !!}</td>
                    </tr>
                    <tr>
                        <td>   Entry criticism  </td>
                        <td> {{ $entry->criticism  }}</td>

                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
@stop

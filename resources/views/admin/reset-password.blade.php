@extends('admin.layouts.main',[
                                'page_header'       => trans('cpanel.users'),
                                'page_description'  => trans('cpanel.admin.change_password')
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">

{!! Form::open([
                            'action'=>'UserController@changePasswordSave',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST'
                            ])!!}

  <div class="box-body">
        @include('flash::message')
        @include('admin.layouts.partials.validation-errors')
        <div class="form-group">
            <label for="email">{{trans('cpanel.admin.email')}}</label>
            {!! Form::text('email',auth()->user()->email,['class' => 'form-control']) !!}
        </div>
        {!! Form::label('old-password', trans('cpanel.admin.current_password')) !!}
        {!! Form::password('old-password',['class' => 'form-control']) !!}

         {!! Form::label('password', trans('cpanel.admin.new_password')) !!}
        {!! Form::password('password',['class' => 'form-control']) !!}

        {!! Form::label('password_confirmation',trans('cpanel.admin.confirm_new_password')) !!}
        {!! Form::password('password_confirmation',['class' => 'form-control']) !!}

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{trans('cpanel.save')}}</button>
        </div>

    </div>

     {!! Form::close()!!}
    <div>
@endsection

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="{{asset('AdminLTE-2.3.0/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            @if(auth()->user()->hasRole('admin'))
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> {{trans('cpanel.home')}} </a></li>

            <li><a href="{{url('admin/slider')}}"><i class="fa fa-image"></i> {{trans('cpanel.sliders.title')}}</a></li>
            <li><a href="{{url('admin/category')}}"><i class="fa fa-image"></i> {{trans('cpanel.categories.title')}}</a></li>

            @endif
                <li><a href="{{url('admin/book')}}"><i class="fa fa-book"></i>  Books </a></li>
            {{--<li><a href="{{url('admin/entry')}}"><i class="fa fa-comment-o"></i>  Entries </a></li>--}}
            <li><a href="{{url('admin/client')}}"><i class="fa fa-users"></i>  Clients </a></li>
                @if(auth()->user()->hasRole('admin'))
                <li><a href="{{url('admin/setting')}}"><i class="fa fa-cogs"></i> {{trans('cpanel.settings.name')}}</a></li>
            <li><a href="{{url('admin/user')}}"><i class="fa fa-key"></i> Users </a></li>
            <li><a href="{{url('admin/user/change-password')}}"><i class="fa fa-key"></i>{{trans('cpanel.admin.change_password')}}</a></li>
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

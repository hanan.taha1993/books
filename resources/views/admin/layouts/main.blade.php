@include('admin.layouts.includes.header')
@include('admin.layouts.includes.sidebar')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{$page_header}}
            <small>{!! $page_description !!}</small>
        </h1>
    </section>

    <section class="content">

        @yield('content')

    </section>
</div>

@include('admin.layouts.includes.footer')
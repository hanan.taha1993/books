@extends('admin.layouts.main',[
                                'page_header'       => config('app.name') ,
                                'page_description'  => 'Control Panel'
                                ])

@inject('book','App\Models\Book')
@inject('client','App\Models\Client')
@section('content')
        <!-- Info boxes -->
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua">
                <i class="fa fa-users" aria-hidden="true"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text"> approved Clients   </span>
                <span class="info-box-number"> {{$client->where('state','approved')->count()}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua">
                <i class="fa fa-users" aria-hidden="true"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text"> Non Approved clients  </span>
                <span class="info-box-number">{{$client->where('state','non_approved')->count()}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

 <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua">
                <i class="fa fa-book" aria-hidden="true"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Books </span>
                <span class="info-box-number">{{$book->count()}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua">
                <i class="fa fa-book" aria-hidden="true"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Suggested Books </span>
                <span class="info-box-number">{{$book->where('state','pending')->count()}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

</div>
<!-- /.row -->
@endsection
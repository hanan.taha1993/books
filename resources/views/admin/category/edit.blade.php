@extends('admin.layouts.main',[
                                'page_header'       => 'categories',
                                'page_description'  => 'Edit category'
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['CategoryController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT',
                            'files'=>true
                            ])!!}

    <div class="box-body">

        @include('admin.category.form')

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection
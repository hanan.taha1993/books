@extends('admin.layouts.main',[
								'page_header'		=> 'categories',
								'page_description'	=> 'show categories'
								])
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <div class="pull-left">
                <a href="{{url('admin/category/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i> New Category 
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="box-body">
                    @include('flash::message')

        @if(!empty($categories) && count($categories)>0)
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th> Arabic name </th>
                <th> English name </th>
                <th class="text-center">Edit</th>
                <th class="text-center">Save</th>
                </thead>
                <tbody>
                    @php $count = 1; @endphp
                    @foreach($categories as $category)
                        <tr id="removable{{$category->id}}">
                            <td>{{$count}}</td>
                            <td>{{$category->ar_name}}</td>
                            <td>{{$category->en_name}}</td>
                            <td class="text-center"><a href="category/{{$category->id}}/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                             <td class="text-center">
                                <button id="{{$category->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('category.destroy',$category->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                        @php $count ++; @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
            {!! $categories->render() !!}
        @else
 <div>
                    <h3 class="text-info" style="text-align: center">No Data To Show </h3>
                </div>
                        @endif
    </div>
</div>
@stop
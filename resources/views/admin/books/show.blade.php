@extends('admin.layouts.main',[
								'page_header'		=> 'books',
								'page_description'	=> 'book  details'
								])
@section('content')
    <div class="box box-primary">
        <div class="box-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="data-table table table-bordered">
                    <tbody>
                    <tr>
                        <td>English book Name </td>
                        <td>{{$book->en_name}}</td>
                    </tr>
                    <tr>
                        <td>Arabic book Name </td>
                        <td>{{$book->ar_name}}</td>
                    </tr>
                    <tr>
                        <td> book author</td>
                        <td>{{$book->author ?? '-'}}</td>
                    </tr>
                     <tr>
                        <td> book Category</td>
                                                <td> {{$book->category->en_name ?? '-'}}</td>
                    </tr>

                    <tr>
                        <td> Client Name</td>
                        @if(count($book->client))
                        <td>{{$book->client->name ?? '-'}}</td>
                        @else
                        <td> added by admin </td>
                        @endif
                    </tr>
                    <tr>
                        <td> English book description</td>
                        <td>{!! $book->en_description !!}</td>
                    </tr>
                    <tr>
                        <td> Arabic book description</td>
                        <td>{!! $book->ar_description !!}</td>
                    </tr>
                    @if(auth()->user()->hasRole('admin'))
                        <tr>
                        <td>  book state</td>
                        <td>
                            @if($book->state == 'approved')
                                <a href="{{url('admin/book/'. $book->id.'/un-approve')}}" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> Pending</a>
                            @else
                                <a href="{{url('admin/book/'.$book->id.'/approve')}}" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Approved</a>
                            @endif
                        </td>
                    </tr>
                        @endif
                    <tr>
                        <td>   Book Cover </td>
                        <td class="text-center">
                            <a href="{{asset($book->thumbnail)}}" data-lightbox="image-1" data-title="{{$book->name}}">
                                <img src="{{asset($book->thumbnail)}}" alt="" style="height: 70px;"></a> </td>

                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            @if(count($book->entries)>0)
                <h3>Entries</h3>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="data-table table table-bordered">
                                <thead>
                                <th>#</th>
                                <th>  client name</th>
                                <th>  summary </th>
                                @if(auth()->user()->hasRole('judge'))
                                    <th class="text-center">Rate</th>
                                    @endif
                                    @if(auth()->user()->hasRole('admin'))
                                    <th class="text-center">Delete</th>
                                    @endif
                                <th class="text-center">More</th>
                                </thead>
                                <tbody>
                                @foreach($entries as $item)
                                    <tr id="removable{{$item->id}}">
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{optional($item->client)->name ?? 'no name !!'}} {{$item->client->l_name}}</td>
                                        <td>{{$item->summary}}</td>
                                        @if(auth()->user()->hasRole('judge'))
                                        <td class="text-center">
                                            <a href="{{url('admin/entry/'.$item->id .'/edit')}}" class="btn btn-xs btn-success"><i
                                                        class="fa fa-edit"></i></a>
                                        </td>
                                       
                                        @endif
                                        <td class="text-center">
                                <button id="{{$item->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('entry.destroy',$item->id)}}"   type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                            </td>
                                        <td class="text-center">
                                            <a href="{{url('admin/entry/'.$item->id )}}" class="btn btn-xs btn-info"><i
                                                        class="fa fa-info-circle"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop

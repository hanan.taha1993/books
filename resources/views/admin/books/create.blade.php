@extends('admin.layouts.main',[
                                'page_header'       => ' Books',
                                'page_description'  => '  New Book'
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'BookController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files'=>true
                            ])!!}

    <div class="box-body">

        @include('admin.books.form')

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection
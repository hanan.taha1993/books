@include('admin.layouts.partials.validation-errors')
@include('flash::message')
@inject('category','App\Models\Category')

{!! \App\Ibnfarouk\MyClasses\Field::text('en_name','English Name ') !!}
{!! \App\Ibnfarouk\MyClasses\Field::text('ar_name','Arabic Name ') !!}
{!! \App\Ibnfarouk\MyClasses\Field::text('author',' Author Name ') !!}

  {!! \App\Ibnfarouk\MyClasses\Field::select('category_id',' choose category',$category->pluck('en_name','id')->toArray()) !!}

{!! \App\Ibnfarouk\MyClasses\Field::fileWithPreview('thumbnail','Thumbnail ') !!}
@if($model->thumbnail)
    <div class="col-md-4">
        <img src="{{url($model->thumbnail)}}" alt="" class="img-responsive thumbnail">
    </div>
@endif
<div class="clearfix"></div>
{!! \App\Ibnfarouk\MyClasses\Field::fileWithPreview('file','File ') !!}
@if($model->file)
        <a href="{{url($model->file)}}"  class="btn btn-info " target="_blank"><i class="fa fa-book"></i> view File </a>
@endif

<div class="clearfix"></div>
{!! \App\Ibnfarouk\MyClasses\Field::textarea('en_description','English description ') !!}
{!! \App\Ibnfarouk\MyClasses\Field::textarea('ar_description','Arabic description ') !!}


@extends('admin.layouts.main',[
								'page_header'		=> 'books',
								'page_description'	=> 'show books'
								])
@section('content')
    <div class="box box-primary">
        <div class="box-header">
                                    @if(auth()->user()->hasRole('admin'))
            <div class="pull-left">
                <a href="{{url('admin/book/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i>New Book
                </a>
            </div>
            @endif
            <div class="clearfix"> </div>
            <div class="row">
            {!! Form::open([
                'method' => 'GET'
            ]) !!}
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('name',\Request::input('name'),[
                        'class' => 'form-control',
                        'placeholder' => 'book name '
                    ]) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::select('state',['approved'=>'approved' ,'pending'=>'pending'],\Request::input('state'),
                    [    'class' => 'form-control',
                        'placeholder' => 'state'
                    ]) !!}
                </div>
            </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button class="btn btn-flat btn-block btn-primary">search</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="box-body">
            @include('flash::message')
        @if(!empty($books) && count($books)>0)
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th> English Name</th>
                <th> Arabic Name</th>
                <th>  Category </th>
                @if(auth()->user()->hasRole('admin'))
                    <th class="text-center">approved /pending</th>
                    @endif
                <th> thumbnail </th>
                <th class="text-center">Info</th>
                @if(auth()->user()->hasRole('admin'))
                    <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
                    @endif
                </thead>
                <tbody>
                    @php $count = 1; @endphp
                    @foreach($books as $book)
                        <tr id="removable{{$book->id}}">
                            <td>{{$count}}</td>
                            <td> {{$book->en_name}}</td>
                            <td> {{$book->ar_name}}</td>
                            <td> {{$book->category->en_name ?? '-'}}</td>
                        @if(auth()->user()->hasRole('admin'))
                            <td class="text-center">
                                @if($book->state == 'approved')
                                    <a href="book/{{$book->id}}/un-approve" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> Un approve</a>
                                @else
                                    <a href="book/{{$book->id}}/approve" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Approve</a>
                                @endif
                            </td>
                            @endif
                            <td class="text-center">
                                <a href="{{asset($book->thumbnail)}}" data-lightbox="image-1" data-title="{{$book->name}}">
                                    <img src="{{asset($book->thumbnail)}}" alt="" style="height: 70px;"></a> </td>
                            <td class="text-center"><a href="{{url('admin/book/'.$book->id)}}" class="btn btn-xs btn-info"><i class="fa fa-info"></i></a></td>
                            @if(auth()->user()->hasRole('admin'))
                            <td class="text-center"><a href="{{url('admin/book/'.$book->id. '/edit')}}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                             <td class="text-center">
                                <button id="{{$book->id}}" data-token="{{ csrf_token() }}" data-route="{{URL::route('book.destroy',$book->id)}}"  type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                            </td>
                                @endif
                        </tr>
                        @php $count ++; @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
            {!! $books->render() !!}
        @else
                <div>
                    <h3 class="text-info" style="text-align: center">No Data To Show </h3>
                </div>
            @endif
    </div>
</div>
@stop
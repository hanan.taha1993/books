@include('admin.layouts.partials.validation-errors')
@include('flash::message')
@inject('role','App\Models\Role')
<?php
$roles = $role->pluck('display_name', 'id')->toArray();
?>
{!!  \App\Ibnfarouk\MyClasses\Field::text('name' , 'Name') !!}
{!!  \App\Ibnfarouk\MyClasses\Field::email('email','email') !!}
{!!  \App\Ibnfarouk\MyClasses\Field::password('password',' password') !!}
{!!  \App\Ibnfarouk\MyClasses\Field::password('password_confirmation', 'password confirmation ') !!}
{!!  \App\Ibnfarouk\MyClasses\Field::multiSelect('roles',' roles' , $roles  ) !!}




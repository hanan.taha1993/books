@extends('admin.layouts.main',[
                                'page_header'       => 'users',
                                'page_description'  => ' add user'
                                ])

@section('content')

<div class="box">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'UserController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST'
                            ])!!}
    <div class="box-body">
        @include('admin.users.form')
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">save</button>
    </div>
    {!! Form::close()!!}
</div>
@stop
@extends('admin.layouts.main',[
                                'page_header'       => 'Users',
                                'page_description'  => ' New User'
                                ])
@section('content')
    <div class="box box-danger">
        <div class="box-body">
            <div class="pull-left">
                <a href="{{url('admin/user/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i>  New User
                </a>
            </div>
            <div class="clearfix"></div>
            <br>
            @include('flash::message')
            @if(!empty($users))
                <div class="table-responsive">
                    <table class="data-table table table-bordered">
                        <thead>
                        <th>#</th>
                        <th> name </th>
                        <th>email </th>
                        <th>Role</th>
                        <th class="text-center">Edit</th>
                        <th class="text-center">Delete</th>
                        </thead>
                        <tbody>
                        @php $count = 1; @endphp
                        @foreach($users as $user)
                            <tr id="removable{{$user->id}}">
                                <td>{{$count}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @foreach($user->roles as $role)
                                        <span class="label label-success">{{$role->display_name}}</span>
                                    @endforeach
                                </td>
                                <td class="text-center"><a href="user/{{$user->id}}/edit"
                                                           class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                                <td class="text-center">
                                    <button id="{{$user->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('user.destroy',$user->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                                class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                            @php $count ++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    {!! $users->render() !!}
                </div>
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
@stop
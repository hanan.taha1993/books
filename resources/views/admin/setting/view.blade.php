@extends('admin.layouts.main',[
                                'page_header'       => trans('cpanel.settings.name'),
                                'page_description'  => trans('cpanel.settings.desc')
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['SettingController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'Post',
                            'files'=>true
                            ])!!}

    <div class="box-body">

        @include('admin.setting.form',compact('model'))

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{trans('cpanel.save')}}</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection
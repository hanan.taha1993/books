@include('admin.layouts.partials.validation-errors')
@include('flash::message')
{!! \App\Ibnfarouk\MyClasses\Field::text('en_site_name' ,'Site Name in English ') !!}
{!! \App\Ibnfarouk\MyClasses\Field::text('ar_site_name' ,'Site Name in Arabic ') !!}


<h3>  {{ trans('cpanel.settings.social_media_accounts')}} </h3>
{!! \App\Ibnfarouk\MyClasses\Field::text('facebook' , trans('cpanel.settings.facebook')) !!}
{!! \App\Ibnfarouk\MyClasses\Field::text('twitter' , trans('cpanel.settings.twitter')) !!}
{!! \App\Ibnfarouk\MyClasses\Field::text('linked_in' , trans('cpanel.settings.linked_in')) !!}

<hr class="box box-primary">

{!! \App\Ibnfarouk\MyClasses\Field::number('share_limit' , 'Maximum Entries Number') !!}
<div class="clearfix"></div>
{!! \App\Ibnfarouk\MyClasses\Field::fileWithPreview('about_contest_file','About contest') !!}
@if($model->about_contest_file)
        <a href="{{url($model->about_contest_file)}}"  class="btn btn-info " target="_blank"><i class="fa fa-book"></i> view File </a>
@endif
<div class="clearfix"></div>
{!! \App\Ibnfarouk\MyClasses\Field::fileWithPreview('rating_item_file','rating item') !!}
@if($model->rating_item_file)
        <a href="{{url($model->rating_item_file)}}"  class="btn btn-info " target="_blank"><i class="fa fa-book"></i> view File </a>
@endif

<hr class="box box-primary">
{!! \App\Ibnfarouk\MyClasses\Field::editor('en_about_us','About us in English ') !!}
{!! \App\Ibnfarouk\MyClasses\Field::editor('ar_about_us','About us in Arabic ') !!}

{{--<hr class="box box-primary">--}}
{{--{!! \App\Ibnfarouk\MyClasses\Field::editor('share_content',trans('cpanel.settings.share_content')) !!}--}}



@extends('admin.layouts.main',[
                                'page_header'       => 'Clients',
                                'page_description'  => 'Client details'
                                ])
@section('content')
    <div class="box box-primary">
        <div class="box-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="data-table table table-bordered">
                    <tbody>
                    <tr>
                        <td> Client Name</td>
                        <td>{{$client->name}} </td>
                    </tr>

                    <tr>
                        <td> Client Phone</td>
                        <td>{{$client->phone}} </td>
                    </tr>

                    <tr>
                        <td>Client Email</td>
                        <td>{{$client->email ?? '-' }}</td>
                    </tr>
                    <tr>
                        <td> Client Speciality</td>
                        <td>{{$client->speciality}} </td>
                    </tr>

                    <tr>
                        <td> Client Nationality</td>
                        <td>{{$client->nationality}} </td>
                    </tr>
                    <tr>
                        <td> Client Gender</td>
                        <td>{{$client->gender}} </td>
                    </tr>
                    <tr>

                        <td>  state </td>
                        <td>
                            @if($client->state == 'approved')
                                <a href="client/{{$client->id}}/un-approve" class="btn btn-xs btn-danger"><i
                                            class="fa fa-close"></i> deActivate</a>
                            @else
                                <a href="client/{{$client->id}}/approve" class="btn btn-xs btn-success"><i
                                            class="fa fa-check"></i> Activate</a>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            @if(count($entries)>0)
                <h3>Client Entries</h3>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="data-table table table-bordered">
                                <thead>
                                <th>#</th>
                                <th>  book name</th>
                                <th>  summary </th>
                                <th class="text-center">More</th>
                                </thead>
                                <tbody>
                                @foreach($entries as $item)
                                    <tr id="removable{{$item->id}}">
                                        <td>{{$loop->iteration}}</td>
                                        <td><a href="{{url('admin/book/'.$item->book->id) }}">{{$item->book->name ?? '-' }}</a></td>
                                        <td>{{$item->summary ?? '-'  }}</td>
                                        <td class="text-center">
                                            <a href="{{url('admin/entry/'.$item->id )}}" class="btn btn-xs btn-info"><i
                                                        class="fa fa-info-circle"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>

            @endif
        </div>
    </div>
@stop
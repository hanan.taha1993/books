@extends('admin.layouts.main',[
                                'page_header'       => 'Client',
                                'page_description'  => ' Edit Client'
                                ])

@section('content')
        <!-- FILE: app/views/start.blade.php -->
<div class="box box-danger">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['ClientController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'PUT'
                            ])!!}
    <div class="box-body">
        @include('admin.clients.form')
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">save</button>
    </div>
    {!! Form::close()!!}
</div>
@stop
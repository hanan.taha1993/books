@extends('admin.layouts.main',[
                                'page_header'       => 'Clients ',
                                'page_description'  => ' New Client'
                                ])
@section('content')
    <div class="box box-primary">
        <div class="box-body">
                                    @if(auth()->user()->hasRole('admin'))
                <div class="pull-left">
                <a href="{{url('admin/client/create')}}" class="btn btn-primary">
                <i class="fa fa-plus"></i>  New Client
                </a>
            </div>
            @endif
            <div class="clearfix"></div>
            <div class="row">
                {!! Form::open([
                    'method' => 'GET'
                ]) !!}
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        {!! Form::text('name',\Request::input('name'),[
                            'class' => 'form-control',
                            'placeholder' => ' name '
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        {!! Form::text('email',\Request::input('email'),[
                            'class' => 'form-control',
                            'placeholder' => 'email  '
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        {!! Form::select('state',['approved'=>'approved' ,'non_approved'=>'non approved'],\Request::input('state'),
                        [    'class' => 'form-control',
                            'placeholder' => 'state'
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button class="btn btn-flat btn-block btn-primary">search</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <br>
            @include('flash::message')
            @if(!empty($clients) && count($clients)>0)
                <div class="table-responsive">
                    <table class="data-table table table-bordered">
                        <thead>
                        <th>#</th>
                        <th>Full Name</th>
                        <th>Phone</th>
                        <th>Email </th>
                        @if(auth()->user()->hasRole('admin'))
                            <th class="text-center">Active /  deActive</th>
                            @endif
                        <th class="text-center">Info</th>
                        @if(auth()->user()->hasRole('admin'))
                            <th class="text-center">Edit </th>
                        <th class="text-center">Delete</th>
                            @endif
                        </thead>
                        <tbody>
                        @php $count = 1; @endphp
                        @foreach($clients as $client)
                            <tr id="removable{{$client->id}}">
                                <td>{{$count}}</td>
                                <td>{{$client->name}}</td>
                                <td>{{$client->phone}}</td>
                                <td>{{$client->email}}</td>
                                @if(auth()->user()->hasRole('admin'))
                                <td class="text-center">
                                    @if($client->state == 'approved')
                                        <a href="client/{{$client->id}}/un-approve" class="btn btn-xs btn-danger"><i
                                                    class="fa fa-close"></i> deActivate</a>
                                    @else
                                        <a href="client/{{$client->id}}/approve" class="btn btn-xs btn-success"><i
                                                    class="fa fa-check"></i> Activate</a>
                                    @endif
                                </td>
                                @endif
                                <td class="text-center"><a href="{{url('admin/client/'.$client->id)}}" class="btn btn-xs btn-info"><i class="fa fa-info"></i></a></td>
                                @if(auth()->user()->hasRole('admin'))
                                <td class="text-center">
                                    <a href="client/{{$client->id}}/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                </td>

                                <td class="text-center">
                                    <button id="{{$client->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('client.destroy',$client->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                                class="fa fa-trash-o"></i></button>
                                </td>
                                    @endif
                            </tr>
                            @php $count ++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    {!! $clients->render() !!}
                </div>
                @else
                <div>
                    <h3 class="text-info" style="text-align: center">No Clients To Show </h3>
                </div>
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
@stop
@extends('admin.layouts.main',[
                                'page_header'       => 'Clients',
                                'page_description'  => ' New Client'
                                ])
@section('content')
<div class="box box-danger">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'ClientController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST'
                            ])!!}
    <div class="box-body">
        @include('admin.clients.form')
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">save</button>
    </div>
    {!! Form::close()!!}
</div>
@stop
@component('mail::message')

@component('mail::button',
['url' => url('api/v1/client/verify?code=' . $user->code)]
)
رجاء الضغط على الرابط التالي لتأكيد بريدك الالكتروني 
@endcomponent

{{ config('app.name') }}
@endcomponent

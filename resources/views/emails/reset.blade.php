@component('mail::message')

@component('mail::button',
['url'=>url('new-password?code='.$user->code .'&email='.$user->email)]
)
إعادة كلمة المرور
@endcomponent
تحياتنا<br>
{{ config('app.name') }}
@endcomponent
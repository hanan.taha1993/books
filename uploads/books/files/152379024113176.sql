-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 01, 2018 at 01:39 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.2.3-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `add_mission`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisment_ads`
--

CREATE TABLE `advertisment_ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ar_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marketing_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `budget` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `ad_period` int(11) NOT NULL,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci,
  `en_description` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `advertisment_ads`
--

INSERT INTO `advertisment_ads` (`id`, `created_at`, `updated_at`, `ar_name`, `en_name`, `category_id`, `client_id`, `city`, `country`, `marketing_id`, `longitude`, `latitude`, `budget`, `start_date`, `end_date`, `ad_period`, `ar_description`, `en_description`) VALUES
(1, '2018-03-14 22:00:00', '2018-03-14 22:00:00', 'test', 'test', '1', '1', 'cairo', 'egypt', '1', 20, 20, 20, '2018-03-31', '2018-03-31', 3, 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` date NOT NULL,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci,
  `en_description` longtext COLLATE utf8mb4_unicode_ci,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categorables`
--

CREATE TABLE `categorables` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `categorable_id` int(11) NOT NULL,
  `categorable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorables`
--

INSERT INTO `categorables` (`id`, `created_at`, `updated_at`, `category_id`, `categorable_id`, `categorable_type`) VALUES
(1, NULL, NULL, 3, 2, 'App\\Models\\Sponosor'),
(2, NULL, NULL, 1, 2, 'service'),
(3, NULL, NULL, 2, 2, 'service');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ar_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `ar_title`, `en_title`, `model`) VALUES
(1, '2018-03-31 13:02:38', '2018-03-31 13:02:38', 'التصنيف الاول', 'category1', 'service'),
(2, '2018-03-31 13:03:02', '2018-03-31 13:03:02', 'التصنيف التانى', 'second category', 'service'),
(3, '2018-03-31 13:03:30', '2018-03-31 13:03:30', 'التصنيف التلت', 'third category', 'sponsor');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accept_condition` tinyint(1) NOT NULL,
  `type` enum('marketer','advertiser','both') COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` enum('pending','confirmed','stopped','unverified') COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `created_at`, `updated_at`, `name`, `phone`, `email`, `city`, `country`, `gender`, `thumbnail`, `password`, `accept_condition`, `type`, `state`, `api_token`, `code`) VALUES
(1, '2018-03-30 22:00:00', '2018-03-30 22:00:00', 'ahmed ali', '01013878433', 'test@test.com', 'cairo', 'egypt', 'male', NULL, '', 1, 'marketer', 'pending', NULL, NULL),
(2, '2018-03-30 22:00:00', '2018-03-30 22:00:00', 'sayes ali', '01013878433', 'test2yahoo.com', 'Giza', 'egypt', 'male', NULL, '', 1, 'advertiser', 'pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marketing_ads`
--

CREATE TABLE `marketing_ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ar_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marketing_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `budget` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `ad_period` int(11) NOT NULL,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci,
  `en_description` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_02_22_000000_create_users_table', 1),
(2, '2018_02_22_083516_create_taggables_table', 1),
(3, '2018_02_22_083516_create_tags_table', 1),
(4, '2018_02_22_100000_create_password_resets_table', 1),
(6, '2018_02_22_121238_create_books_table', 1),
(7, '2018_02_22_121238_create_courses_table', 1),
(8, '2018_02_22_121238_create_instructors_table', 1),
(9, '2018_02_22_121238_create_jobs_table', 1),
(10, '2018_02_22_121238_create_learing_projects_table', 1),
(11, '2018_02_22_121238_create_links_table', 1),
(12, '2018_02_22_121238_create_packages_table', 1),
(13, '2018_02_22_121238_create_pages_table', 1),
(14, '2018_02_22_121238_create_posts_table', 1),
(15, '2018_02_22_121238_create_settings_table', 1),
(16, '2018_02_22_121238_create_sponsors_table', 1),
(17, '2018_02_22_121238_create_subscribers_table', 1),
(18, '2018_02_22_121238_create_tutorials_table', 1),
(19, '2018_02_22_121238_create_twetter_feeeds_table', 1),
(20, '2018_02_22_121238_create_videos_table', 1),
(21, '2018_02_20_083515_create_contacts_table', 2),
(22, '2018_02_20_083516_create_services_table', 2),
(23, '2018_01_16_134249_create_photos_table', 3),
(28, '2018_03_31_115247_create_blogs_table', 4),
(33, '2018_03_31_132553_create_categories_table', 5),
(34, '2018_03_31_132607_create_types_table', 5),
(35, '2018_03_31_145443_typeable', 5),
(36, '2018_03_31_145534_categorable', 5),
(41, '2018_03_31_154507_create_clients_table', 6),
(42, '2018_03_31_174710_create_advertisment_ads_table', 6),
(43, '2018_03_31_174724_create_marketing_ads_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_order` int(11) NOT NULL,
  `published_at` datetime NOT NULL,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `en_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_description` text COLLATE utf8mb4_unicode_ci,
  `en_slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `deleted_at`, `ar_name`, `ar_slug`, `page_order`, `published_at`, `ar_description`, `thumbnail`, `user_id`, `en_name`, `en_description`, `en_slug`) VALUES
(1, '2018-03-26 18:55:20', '2018-03-26 18:55:48', '2018-03-26 18:55:48', 'المملكة المتحدة', NULL, 1, '2018-03-29 00:00:00', 'w<p><br></p>', 'uploads/pages/152209772017093.jpg', 1, 'united king', '<p>e<br></p>', NULL),
(2, '2018-03-27 06:11:37', '2018-03-27 06:11:38', NULL, 'المملكة المتحدة', NULL, 1, '2018-03-27 00:00:00', '<p>test<br></p>', 'uploads/pages/152213829893955.png', 1, 'united king', '<p>test<br></p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photoable_id` int(11) NOT NULL,
  `photoable_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `created_at`, `updated_at`, `url`, `photoable_id`, `photoable_type`, `extension`) VALUES
(1, '2018-03-27 08:00:54', '2018-03-27 08:00:54', '/uploads/Advertise_ad/152214485453150.png', 1, 'App\\Models\\Advertisment_Ad', 'png'),
(2, '2018-03-27 08:00:54', '2018-03-27 08:00:54', '/uploads/Advertise_ad/152214485434248.jpeg', 1, 'App\\Models\\Advertisment_Ad', 'jpeg'),
(4, '2018-03-27 08:00:54', '2018-03-27 08:00:54', '/uploads/Advertise_ad/152214485460677.png', 1, 'App\\Models\\Advertisment_Ad', 'png'),
(5, '2018-03-31 13:20:51', '2018-03-31 13:20:51', 'uploads/services/152250965168845.jpeg', 2, 'service', 'jpeg'),
(6, '2018-03-31 13:20:51', '2018-03-31 13:20:51', 'uploads/services/152250965118892.jpeg', 2, 'service', 'jpeg'),
(8, '2018-03-31 13:20:51', '2018-03-31 13:20:51', 'uploads/advertise_ads/152250965118892.jpeg', 1, 'advertise_ad', 'jpeg'),
(9, '2018-03-31 13:20:51', '2018-03-31 13:20:51', 'uploads/advertise_ads/152250965168845.jpeg', 1, 'advertise_ad', 'jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` date NOT NULL,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci,
  `en_description` longtext COLLATE utf8mb4_unicode_ci,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `created_at`, `updated_at`, `deleted_at`, `ar_name`, `en_name`, `ar_slug`, `en_slug`, `published_at`, `ar_description`, `en_description`, `thumbnail`) VALUES
(2, '2018-03-27 08:00:54', '2018-03-27 08:00:54', NULL, 'المملكة المتحدة', 'united king', '', 'united_king', '2018-03-27', '<p>tttttttttttttt<br></p>', '<p>gggggggggg<br></p>', 'uploads/services/152214485476187.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linked_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_site_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_site_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8mb4_unicode_ci,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci,
  `en_description` longtext COLLATE utf8mb4_unicode_ci,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_working_hours` longtext COLLATE utf8mb4_unicode_ci,
  `en_working_hours` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `created_at`, `updated_at`, `facebook`, `twitter`, `youtube`, `linked_in`, `google_plus`, `instagram`, `ar_site_name`, `en_site_name`, `email`, `ar_description`, `en_description`, `keywords`, `longitude`, `latitude`, `phone`, `fax`, `ar_working_hours`, `en_working_hours`) VALUES
(1, '2018-03-26 17:26:35', '2018-03-31 10:42:50', 'https://www.facebook.com/', 'https://www.facebook1.com/', 'https://www.facebook2.com/', 'https://www.facebook5.com/', 'https://www.facebook3.com/', 'https://www.facebook4.com/', 'مهمة الإعلانات', 'add Mission', 'test@test.com', '<p>وصف الموقع بالعربية<br></p>', '<p>website description<br></p>', 'test', 20, 20, '01013878433', '2222222', '<p>hhhhhhh<br></p>', '<p>&nbsp;&nbsp;&nbsp; hhhhh<br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ar_name` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `en_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `created_at`, `updated_at`, `deleted_at`, `ar_name`, `thumbnail`, `ar_description`, `url`, `published_at`, `user_id`, `en_name`, `en_description`) VALUES
(1, '2018-03-26 18:22:23', '2018-03-26 18:44:22', '2018-03-26 18:44:22', 'المملكة الsمتحدة', '/uploads/sponsors/152209613555069.png', '<p>الوصف بالعربية    <br></p>', 'http://phpmyadmin.test/tbl_structure.php?db=add_mission&table=sponsors&token=05b9a8e044994a7ded35b1e2cc66ae24', '2018-03-26 00:00:00', 0, 'united king', '<p>english description<br></p>'),
(2, '2018-03-31 12:26:12', '2018-03-31 18:59:12', '2018-03-31 18:59:12', 'المملكة المتحدة', '', '<p>test    <br></p>', 'http://crops.test/', '2018-03-31 00:00:00', 0, 'united king', '<p>test<br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `created_at`, `updated_at`, `deleted_at`, `email`) VALUES
(1, '2018-03-25 22:00:00', '2018-03-26 17:36:43', '2018-03-26 17:36:43', 'test@test.com'),
(2, '2018-03-25 22:00:00', '2018-03-25 22:00:00', NULL, 'test2@test.com');

-- --------------------------------------------------------

--
-- Table structure for table `taggables`
--

CREATE TABLE `taggables` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tag_id` int(11) NOT NULL,
  `taggable_id` int(11) NOT NULL,
  `taggable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `taggables`
--

INSERT INTO `taggables` (`id`, `created_at`, `updated_at`, `tag_id`, `taggable_id`, `taggable_type`) VALUES
(1, NULL, NULL, 2, 2, 'page'),
(2, NULL, NULL, 2, 2, 'App\\Models\\Service'),
(5, NULL, NULL, 2, 2, 'service');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ar_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `created_at`, `updated_at`, `deleted_at`, `ar_title`, `en_title`) VALUES
(1, '2018-03-26 17:20:25', '2018-03-26 17:22:34', '2018-03-26 17:22:34', 'ويب1', 'web1'),
(2, '2018-03-26 18:44:14', '2018-03-26 18:44:14', NULL, 'ويب', 'web');

-- --------------------------------------------------------

--
-- Table structure for table `typeables`
--

CREATE TABLE `typeables` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `typeable_id` int(11) NOT NULL,
  `typeable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `typeables`
--

INSERT INTO `typeables` (`id`, `created_at`, `updated_at`, `type_id`, `typeable_id`, `typeable_type`) VALUES
(2, NULL, NULL, 2, 2, 'App\\Models\\Sponosor'),
(3, NULL, NULL, 1, 2, 'service');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ar_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `created_at`, `updated_at`, `ar_title`, `en_title`, `model`) VALUES
(1, '2018-03-31 13:03:50', '2018-03-31 13:03:50', 'الاول', 'first', 'service'),
(2, '2018-03-31 13:04:09', '2018-03-31 13:04:09', 'التانى', 'second', 'sponsor');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `date_of_birth` date NOT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `name`, `phone`, `gender`, `date_of_birth`, `api_token`, `region_id`) VALUES
(1, 'admin@admin.com', '$2y$10$TLlco92N2.D7n9zx0MZJHu5NdKAP9dEQkp7pDRtY9oIZw44EB8l4u', '8htCfkXrBWD9tfsrGst0VdiXirYrEDSU2s4f8EVdblTsEyEctgm4W5qcOJA1', '2018-03-26 17:18:36', '2018-03-26 17:18:36', 'admin', '', 'male', '0000-00-00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` datetime NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisment_ads`
--
ALTER TABLE `advertisment_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorables`
--
ALTER TABLE `categorables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marketing_ads`
--
ALTER TABLE `marketing_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taggables`
--
ALTER TABLE `taggables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeables`
--
ALTER TABLE `typeables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisment_ads`
--
ALTER TABLE `advertisment_ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorables`
--
ALTER TABLE `categorables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marketing_ads`
--
ALTER TABLE `marketing_ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `taggables`
--
ALTER TABLE `taggables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `typeables`
--
ALTER TABLE `typeables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('clients', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->string('nationality')->nullable();
			$table->string('speciality')->nullable();
			$table->string('phone')->nullable();
			$table->enum('gender', array('male', 'female'));
			$table->rememberToken();
			$table->string('email')->nullable();
			$table->string('password')->nullable();
			$table->string('code')->nullable();
			$table->string('api_token')->nullable();
			$table->enum('state', array('approved', 'non_approved'));

		});
	}

	public function down()
	{
		Schema::drop('clients');
	}
}
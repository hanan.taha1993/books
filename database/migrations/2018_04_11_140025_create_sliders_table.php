<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidersTable extends Migration {

	public function up()
	{
		Schema::create('sliders', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('ar_title')->nullable();
			$table->string('en_title')->nullable();
			$table->longText('ar_description');
			$table->longText('en_description');
			$table->string('thumbnail')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('sliders');
	}
}
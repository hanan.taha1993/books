<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEntriesTable extends Migration {

	public function up()
	{
		Schema::create('entries', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('book_id');
			$table->longText('summary')->nullable();
			$table->longText('criticism')->nullable();
			$table->boolean('draft');
			$table->integer('client_id');
		});
	}

	public function down()
	{
		Schema::drop('entries');
	}
}
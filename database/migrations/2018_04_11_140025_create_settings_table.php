<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	public function up()
	{
		Schema::create('settings', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('ar_site_name')->nullable();
			$table->string('en_site_name')->nullable();
			$table->string('facebook')->nullable();
			$table->string('twitter')->nullable();
			$table->string('linked_in')->nullable();
			$table->longText('about_us')->nullable();
			$table->integer('share_limit')->nullable();
			$table->string('about_contest_file')->nullable();
			$table->string('rating_item_file')->nullable();

		});
	}

	public function down()
	{
		Schema::drop('settings');
	}
}
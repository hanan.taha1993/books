<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBooksTable extends Migration {

	public function up()
	{
		Schema::create('books', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('ar_name')->nullable();
			$table->string('en_name')->nullable();
			$table->string('author')->nullable();
			$table->string('file')->nullable();
			$table->longText('ar_description')->nullable();
			$table->longText('en_description')->nullable();
			$table->string('thumbnail')->nullable();
			$table->enum('state', array('approved', 'pending'));
			$table->integer('category_id');
		});
	}

	public function down()
	{
		Schema::drop('books');
	}
}
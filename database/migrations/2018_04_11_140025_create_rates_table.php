<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatesTable extends Migration {

	public function up()
	{
		Schema::create('rates', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('book_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('entry_id')->nullable();
			$table->integer('rate_value')->nullable();

		});
	}

	public function down()
	{
		Schema::drop('rates');
	}
}
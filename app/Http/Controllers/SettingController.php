<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Settings;



class SettingController extends Controller
{
       public function view(Settings $model)
    {
        if ($model->all()->count() > 0)
        {
            $model = Settings::find(1);
        }

        return view('admin.setting.view',compact('model'));
    }

    public function update(Request $request)
    {

    	 $this->validate($request, [
            'facebook'           => 'url',
            'twitter'            => 'url',
            'linked_in'        => 'url',
            'en_about_us'           => 'required',
            'en_site_name'          => 'required',
             'ar_about_us'           => 'required',
             'ar_site_name'          => 'required',
             'share_limit'       => 'required',
        ]);
//        return $request->all();
        if (Settings::all()->count() > 0)
        {
            $record = Settings::find(1)->update($request->except('rating_item_file','about_contest_file'));
             $record = Settings::find(1);
            if($request->hasFile('rating_item_file'))
        {
             if (file_exists($record->rating_item_file))
            {
                unlink($record->rating_item_file); 
            }
            $file = $request->file('rating_item_file');
            $filedestinationPath = public_path().'/uploads/settings';
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $file->move($filedestinationPath, $name); // uploading file to given
            $record->update(['rating_item_file' => 'uploads/settings/'.$name]);
        }

             if($request->hasFile('about_contest_file'))
        {
             if (file_exists($record->about_contest_file))
            {
                unlink($record->about_contest_file); 
            }
            $file = $request->file('about_contest_file');
            $filedestinationPath = public_path().'/uploads/settings';
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $file->move($filedestinationPath, $name); // uploading file to given
            $record->update(['about_contest_file' => 'uploads/settings/'.$name]);
        }


        }else{
            $record = Settings::create($request->except('rating_item_file','about_contest_file'));
                 if($request->hasFile('rating_item_file'))
        {
            $file = $request->file('rating_item_file');
            $filedestinationPath = public_path().'/uploads/settings';
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $file->move($filedestinationPath, $name); // uploading file to given
            $record->update(['rating_item_file' => 'uploads/settings/'.$name]);
        }

             if($request->hasFile('about_contest_file'))
        {
            $file = $request->file('about_contest_file');
            $filedestinationPath = public_path().'/uploads/settings';
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $file->move($filedestinationPath, $name); // uploading file to given
            $record->update(['about_contest_file' => 'uploads/settings/'.$name]);
        }
        }

        flash()->success(trans('cpanel.settings.success'));
        return back();
    }
}

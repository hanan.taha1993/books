<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Entry;
class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $entries = Entry::where(function($q) use($request){
//            if($request->client_name)
//            {
//            }
            if($request->book_id)
            {
                $q->where('book_id',$request->book_id);
            }
        })->with('book','client')->latest()->paginate(20);
        return view('admin.entries.index',compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = Entry::findorfail($id);
        return view('admin.entries.show',compact('entry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Entry::findOrFail($id);
        return view('admin.entries.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'rate_value'             => 'numeric|digits_between:1,100',
        ]);
        $entry = Entry::with('book')->findorfail($id);
        $entry->rates()->create([
            'rate_value'    => $request->rate_value,
            'entry_id'      => $entry->id,
            'book_id'       => $entry->book->id,
            'user_id'       => auth()->user()->id,
        ]);
        return redirect('admin/book/'.$entry->book->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { $book = Entry::findOrFail($id);
        $book->delete();
        $data = [
            'status' => 1,
            'msg' => 'Entry has been deleted successfully',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\Interest;
use App\Models\Slider;
use App\Models\Specialtie;
use Auth;
use Validator;
use App\Models\City;
use App\Models\Contact;
use App\Models\Setting;  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{


    public function cities(Request $request)
    {
       $cities = City::where(function($q) use($request){
           $q->where('status',1);
        })->get();
        return responseJson(1,'تم التحميل',$cities);
    }

    public function interests(Request $request)
    {
        $intersets = Interest::where(function($q) use($request){
            $q->where('status',1);
        })->get();
        return responseJson(1,'تم التحميل',$intersets);
    }


    public function specialties(Request $request)
    {
        $specialties = Specialtie::where(function($q) use($request){
            $q->where('status',1);
        })->get();
        return responseJson(1,'تم التحميل',$specialties);
    }


    public function sliders(Request $request)
    {
        $sliders = Slider::where(function($q) use($request){
            $q->where('status',1);
        })->get();
        return responseJson(1,'تم التحميل',$sliders);
    }
    
    public function settings()
    {
        return responseJson(1,'',Setting::first());
    }

    public function categories()
    {
        $data = [
            [
                'value' => 'halls',
                'text' => 'حجز القاعات والبوفيهات',
            ],
            [
                'value' => 'conferences',
                'text' => 'مؤتمرات ومناسبات',
            ],
            [
                'value' => 'trainer',
                'text' => 'المدربين',
            ],
            [
                'value' => 'institute',
                'text' => 'معاهد ودورات',
            ],
        ];
        return responseJson(1,'تم تحميل البيانات بنجاح',$data);
    }
}

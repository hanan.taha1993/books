<?php

namespace App\Http\Controllers\Api\Client;


use Hash;
use Rule;
use Validator;
use Mail;
use Redirect;
use App\Models\Client;
use App\Mail\VerifyMember;
use App\Mail\resetPassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{

    public function register(Request $request)
    {
        $validation = validator()->make($request->all(), [
            'name' => 'required',
            'city_id' => 'required',
            'password' => 'required|confirmed',
            'mobile' => 'required|unique:clients,mobile',
            'email' => 'required|unique:clients,email',
            'category' => 'required|in:trainer,member,institute,halls_sideboard'
        ]);
        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }
        $Token = str_random(60);
        $code = str_random(6);
        $request->merge(array('code' => $code));
        $request->merge(array('password' => bcrypt($request->password)));
        $request->merge(array('state' => 'unverified'));
        $user = Client::create($request->all());

        if ($request->has('interests')) {
            foreach ($request->interests as $interest) {
                $data = [$interest['id'] => [
                    'client_id' => $user->id,
                ]];
                $user->interests()->attach($data);
            }
        }

        if ($request->has('photos')) {
            $path = base_path();
            foreach ($request->file('photos') as $photo) {
                $destinationPath = $path . '/uploads/clients/'; // upload path
                $extension = $photo->getClientOriginalExtension(); // getting image extension
                $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
                $photo->move($destinationPath, $name); // uploading file to given path
                $user->photos()->create(['url' => '/uploads/clients/' . $name, 'extension' => $extension]);
            }
        }
        if ($request->has('courses'))
            foreach ($request->courses as $course) {
                $user->trainercourses()->create([
                    'city_id' => $course['city_id'],
                    'center_name' => $course['center_name'],
                    'name' => $course['name']
                ]);
            }
        if ($user) {
            $user->api_token = $Token;
            $user->save();
            $data = [
                'api_token' => $user->api_token,
                'code' => $user->code,
                'user' => $user->fresh()->load('photos', 'specialtie', 'city', 'interests', 'trainercourses')
            ];
            Mail::to($user->email)->send(new VerifyMember($user));
            return responseJson(1, 'تم إنشاء الحساب بنجاح , برجاء فحص بريدك الإلكترونى لتفعيل الحساب', $data);
        } else {
            return responseJson(0, 'خطأ إثناء تسجيل حساب جديد');
        }
    }

    public function verify(Request $request)
    {
        $user = Client::where('code', $request->code)->first();
        if ($user)
        {
            if ($user->state == 'unverified') {
                $user->state = 'confirmed';
                $user->code = null;
                $user->save();
                return view('welcome', ['message' => 'تم تفعيل الحساب بنجاح']);
            } else {
                return view('welcome', ['message' => 'حدث خطأ أثناء تفعيل الحساب']);
            }
        }else{
            return view('welcome', ['message' => 'حدث خطأ أثناء تفعيل الحساب']);
        }
    }


    public function login(Request $request)
    {
        $validation = validator()->make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }
        $user = Client::where([
            ['email', $request->email],
            ['state', 'confirmed']
        ])->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $data = [
                    'api_token' => $user->api_token,
                    'user' => $user
                ];
                return responseJson(1, 'تم تسجيل الدخول بنجاح', $data);
            } else {
                return responseJson(0, 'خطأ أثناء تسجيل الدخول');
            }
        } else {
            return responseJson(0, 'خطأ أثناء تسجيل الدخول');
        }
    }

    public function profile(Request $request)
    {

        $validation = validator()->make($request->all(), [
            'mobile' => 'unique:clients,mobile,' . $request->user()->id,
            'email' => 'unique:clients,email,' . $request->user()->id,

        ]);

        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }

        if ($request->has('name')) {
            $request->user()->update($request->only('name'));
        }


        if ($request->has('email')) {
            $request->user()->update($request->only('email'));
        }

        if ($request->has('mobile')) {
            $request->user()->update($request->only('mobile'));
        }


        if ($request->has('place')) {
            $request->user()->update($request->only('place'));
        }

        if ($request->has('longitude')) {
            $request->user()->update($request->only('longitude'));
        }

        if ($request->has('latitude')) {
            $request->user()->update($request->only('latitude'));
        }

        if ($request->has('about_trainer')) {
            $request->user()->update($request->only('about_trainer'));
        }


        if ($request->has('contact_info')) {
            $request->user()->update($request->only('contact_info'));
        }

        if ($request->has('city_id')) {
            $request->user()->update($request->only('city_id'));
        }

        if ($request->has('specialization_id')) {
            $request->user()->update($request->only('specialization_id'));
        }


        if ($request->has('password')) {
            $request->merge(array('password' => bcrypt($request->password)));
            $request->user()->update($request->only('password'));
        }


        if ($request->user()->category == 'trainer' && $request->hasFile('certifications')) {
            $user = $request->user();
            $path = base_path();
            foreach ($request->file('certifications') as $photo) {
                $destinationPath = $path . '/uploads/clients/'; // upload path
                $extension = $photo->getClientOriginalExtension(); // getting image extension
                $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
                $photo->move($destinationPath, $name); // uploading file to given path
                $user->photos()->create(['url' => '/uploads/clients/' . $name, 'extension' => $extension]);
            }

        }

        if ($request->has('interests')) {
            foreach ($request->interests as $interest) {
                $data = [$interest['id'] => [
                    'client_id' => $user->id,
                ]];
                $user->interests()->attach($data);
            }
        }


        if ($request->user()->category == 'trainer')
            $data = $request->user()->fresh()->load('specialtie', 'city', 'trainercourses', 'photos');
        if ($request->user()->category == 'member')
            $data = $request->user()->fresh()->load('specialtie', 'city', 'interests', 'registeredCourses');
        if ($request->user()->category == 'institute')
            $data = $request->user()->fresh()->load('specialtie', 'courses', 'photos');
        if ($request->user()->category == 'halls_sideboard')
            $data = $request->user()->fresh()->load('halls', 'photos');


        return responseJson(1, 'تم تحميل البيانات بنجاح', $data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function reset(Request $request)
    {
        $validation = validator()->make($request->all(), [
            'email' => 'required'
        ]);

        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }

        $user = Client::where('email', $request->email)->first();
        if ($user) {
            $code = rand(111111, 999999);
            $update = $user->update(['code' => $code]);
            if ($update) {
                // send email
                Mail::to($user->email)->send(new resetPassword($user));
                return responseJson(1, 'برجاء فحص بريدك الالكتروني');
            } else {
                return responseJson(0, 'حدث خطأ ، حاول مرة أخرى');
            }
        } else {
            return responseJson(0, 'لا يوجد أي حساب مرتبط بهذا البريد الالكتروني');
        }
    }
    public function newPassword(Request $request)
    {
        return view('admin.client.reset-password');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function password(Request $request)
    {
        $this->validate($request, [
            'code'        =>'required',
            'old_password' => 'required',
            'password'     => 'required|confirmed',
        ]);
        $user = Client::where('code', $request->code)->first();
        if($user)
        {
            if (Hash::check($request->old_password, $user->password)) {
                if ($request->has('password')) {
                        $user->password = bcrypt($request->password);
                        $user->code = null;
                        $user->save();
                     }
                return view('welcome', ['message' => 'تم إعادة تعين كلمة المرور بنجاح']);
            }else{
                flash()->error('خطأ أثناء عملية التحديث');
                return redirect()->back();
            }
        }else{
            flash()->error('خطأ أثناء عملية التحديث');
            return redirect()->back();
        }
    }


}

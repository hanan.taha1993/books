<?php

namespace App\Http\Controllers\Api\Client;

use  Validator;
use App\Models\Hall;
use App\Models\Client;
use App\Models\Trainer_Courses;
use App\Models\Course;
use App\Models\Favourite;
use  Illuminate\Http\Request;
use  App\Http\Controllers\Controller;

class ClientController extends Controller
{

    /***************************************************************************************************/
    //common Services
    public function notifications(Request $request)
    {
        $notifications = $request->user()
            ->notifications()->latest()->paginate(20);
        $request->user()
            ->notifications()->whereIn('id', $notifications->pluck('id')->toArray())->update(['is_read' => 1]);
        return responseJson(1, 'تم تحميل بيانات الإشعارات', $notifications);
    }

    public function contactUs(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ]);
        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }
        $request->user()->contacts()->create(['title' => $request->title, 'content' => $request->input('content')]);
        return responseJson(1, 'تم إرسال الرسالة بنجاح');
    }


    public function showClient(Request $request)
    {
        $user = Client::find($request->id);
        if ($user) {

            if ($user->category == 'trainer')
                $client = $user->fresh()->load('specialtie', 'city', 'trainercourses', 'photos');
            if ($user->category == 'member')
                $client = $user->fresh()->load('specialtie', 'city', 'interests', 'registeredCourses', 'favourites');
            if ($user->category == 'institute')
                $client = $user->fresh()->load('specialtie', 'courses', 'photos');
            if ($user->category == 'halls_sideboard')
                $client = $user->fresh()->load('halls', 'photos', 'favourites');
            if ($user->category == 'conferences')
                $client = $user->fresh()->load('photos');
            return responseJson(1, 'تم التحميل', $client);
        }
        return responseJson(1, 'تم التحميل', $user);
    }


    public function myFavourites(Request $request)
    {
        $myfavourites = $request->user()->favourites()->paginate(20);
        return responseJson(1, 'تم تحميل البيانات بنجاح', $myfavourites);
    }

    public function favorite(Request $request)
    {
        $validation = validator()->make($request->all(), [
            'item_id' => 'required',
            'type' => 'required|in:client,course'
        ]);

        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }

        if($request->type == 'course')
        {
            $item = Course::find($request->item_id);
            $check = $item->favourites()->where(['client_id',$request->client_id])->get();
            if(!$check) {
                $item->favourites()->create(['client_id' => $request->user()->id]);
            }
            else{
                return responseJson(0, ' تمت الإضافة إلى المفضلة من قبل ');
            }
        }
        else{
            $item = Client::find($request->item_id);
            $check = $item->favourites()->where(['client_id',$request->client_id])->get();
            if(!$check){
                $item->favourites()->create(['client_id'=>$request->user()->id]);
            }
            else{
                return responseJson(0, ' تمت الإضافة إلى المفضلة من قبل ');
            }
        }
            return responseJson(1, 'تم الإضافة الى المفضلة');
        }


    public function unfavourite(Request $request)
    {
        $validation = validator()->make($request->all(), [
            'ids.*' => 'required',
            'type' => 'required|in:client,course'
        ]);

        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }
        if ($request->type == 'course') {
            foreach ($request->ids as $item_id) {
                $item = Course::find($item_id);
                if ($item) {
                    $item->favourites()->where('client_id', $request->user()->id)->delete();
                }
            }
            return responseJson(1, 'تم الحذف من المفضلة بنجاح');
        }
        if ($request->type == 'client') {
            foreach ($request->ids as $item_id) {
                $item = Client::find($item_id);
                if ($item) {
                    $item->favourites()->where('client_id', $request->user()->id)->delete();
                }
            }
            return responseJson(1, 'تم الحذف من المفضلة بنجاح');
        }
    }
    /***************************************************************************************************/
    // Member Services
    public function registerInCourse(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'course_name' => 'required',
            'specialization' => 'required',
            'city' => 'required',
        ]);
        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }
        $request->user()->registeredCourses()->create($request->all());
        return responseJson(1, 'تم إرسال طلب التسجيل بنجاح');
    }
    /***************************************************************************************************/
    //  Trainer Services
    public function addCourseByTrainer(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'center_name' => 'required',
            'city_id' => 'required',
        ]);
        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }
        if ($request->user()->category == 'trainer') {
            $request->user()->trainercourses()->create($request->all());
            return responseJson(1, 'تم إضافة الدورة بنجاح');
        } else {
            return responseJson(0, 'ليس لديك صلاحية اضافة دورة');
        }
    }

    public function getTrainerCourses(Request $request)
    {
        $courses = $request->user()->trainercourses()->get();
        return responseJson(1, 'تم تحميل دورات المدرب بنجاح', $courses);
    }

    public function deleteTrainerCourse(Request $request)
    {
        $course = Trainer_Courses::find($request->course_id);
        if ($course) {
            if ($course->photos) {
                foreach ($course->photos as $photo) {
                    unlink($photo);
                }
                $course->photos->delete();
            }
            $course->delete();
            return responseJson(1, 'تم حذف بيانات الدورة بنجاح');
        } else {
            return responseJson(1, 'تم حذف بيانات الدورة من قبل');
        }
    }

    public function filterTrainers(Request $request)
    {
        $trainers = Client::where(function ($query) use ($request) {
            $query->where('category', 'trainer');
            if ($request->has('keyword')) {
                $query->where('gender', 'LIKE', '%' . $request->keyword . '%');
            }
            if ($request->has('city_id')) {
                $query->where('city_id', $request->city_id);
            }

            if ($request->has('specialization_id')) {
                $query->where('specialization_id', $request->city_id);
            }
        })->with('trainercourses', 'city', 'specialtie', 'photos')->paginate(20);
        return responseJson(1, 'تم التحميل', $trainers);
    }
    /*************************************************************************************************/
    //institute services
    public function addCourseByInstitute(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'place' => 'required',
            'date' => 'required|date',
            'time' => 'required',
            'cost' => 'required',
            'attendees' => 'required',
            'available_seats' => 'required',
            'trainer_photo' => 'required|mimes:jpeg,jpg,png',
            'about_trainer' => 'required',
            'description' => 'required',
            'photos.*' => 'mimes:jpeg,jpg,png'
        ]);
        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }

        if ($request->user()->category == 'institute') {
            $path = base_path();
            $course = $request->user()->courses()->create($request->except('trainer_photo'));

            if ($request->has('trainer_photo')) {
                $photo = $request->file('trainer_photo');
                $destinationPath = $path . '/uploads/courses/trainers_photo/'; // upload path
                $extension = $photo->getClientOriginalExtension(); // getting image extension
                $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
                $photo->move($destinationPath, $name); // uploading file to given path
                $course->trainer_photo = 'uploads/courses/trainers_photo/' . $name;
                $course->save();

            }
            if ($request->has('photos')) {
                foreach ($request->file('photos') as $photo) {
                    $destinationPath = $path . '/uploads/courses/courses_photos/'; // upload path
                    $extension = $photo->getClientOriginalExtension(); // getting image extension
                    $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
                    $photo->move($destinationPath, $name); // uploading file to given path
                    $course->photos()->create(['url' => '/uploads/courses/courses_photos/' . $name, 'extension' => $extension]);
                }
            }
            return responseJson(1, 'تم إضافة الدورة بنجاح', $course->fresh()->load('photos'));
        } else {
            return responseJson(0, 'ليس لديك صلاحية اضافة دورة');
        }
    }

    public function getInstituteCourses(Request $request)
    {
        $courses = $request->user()->courses()->with('photos')->get();
        return responseJson(1, 'تم تحميل دورات المعهد بنجاح', $courses);
    }


    public function deleteInstituteCourse(Request $request)
    {
        if (!empty($request->courses) && count($request->courses) > 0) {
            foreach ($request->courses as $item) {
                $course = Course::find($item);
                if ($course) {
                    if ($course->photos) {
                        foreach ($course->photos as $photo) {
                            if (file_exists($photo)) {
                                unlink($photo);
                            }
                        }
                        $course->photos()->delete();
                    }
                    $course->delete();
                }
            }
            return responseJson(1, 'تم حذف بيانات الدورة بنجاح');
        } else {
            return responseJson(0, 'حدث خطأ أثناء محاولة الحذف');
        }
    }


    public function filterInstitute(Request $request)
    {
        $trainers = Client::where(function ($query) use ($request) {
            $query->where('category', 'institute');
            if ($request->has('city_id')) {
                $query->where('city_id', $request->city_id);
            }

            if ($request->has('specialization_id')) {
                $query->where('specialization_id', $request->city_id);
            }
        })->with('city', 'specialtie', 'photos')->paginate(20);
        return responseJson(1, 'تم التحميل', $trainers);
    }

    public function getInstituteCourse(Request $request)
    {
        $course = Course::with('photos')->find($request->id);
        return responseJson(1, 'تم التحميل', $course);

    }
    /***********************************************************************************************/
    //halls_sideboard services
    public function addHall(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required|in:hall,sideboard',
            'place' => 'required',
            'cost' => 'required',
            'available_seats' => 'required',
            'description' => 'required',
            'photos.*' => 'mimes:jpeg,jpg,png'
        ]);
        if ($validation->fails()) {
            $data = $validation->errors();
            return responseJson(0, $validation->errors()->first(), $data);
        }

        if ($request->user()->category == 'halls_sideboard') {
            $path = base_path();
            $hall = $request->user()->halls()->create($request->all());
            if ($request->has('photos')) {
                foreach ($request->file('photos') as $photo) {
                    $destinationPath = $path . '/uploads/halls/'; // upload path
                    $extension = $photo->getClientOriginalExtension(); // getting image extension
                    $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
                    $photo->move($destinationPath, $name); // uploading file to given path
                    $hall->photos()->create(['url' => '/uploads/halls/' . $name, 'extension' => $extension]);
                }
            }
            return responseJson(1, 'تم إضافة إعلان  بنجاح', $hall->fresh()->load('photos'));
        } else {
            return responseJson(0, 'ليس لديك صلاحية اضافة اعلان');
        }
    }

    public function getHalls(Request $request)
    {
        $halls = $request->user()->halls()->with('photos')->get();
        return responseJson(1, 'تم تحميل دورات المعهد بنجاح', $halls);
    }

    public function deleteHall(Request $request)
    {
        $hall = Hall::find($request->id);
        if ($hall) {
            if ($hall->photos) {
                foreach ($hall->photos as $photo) {
                    if (file_exists($photo))
                        unlink($photo);
                }
                $hall->photos()->delete();
            }
            $hall->delete();
            return responseJson(1, 'تم حذف بيانات القاعة بنجاح');
        } else {
            return responseJson(0, 'تم حذف بيانات القاعة من قبل');
        }
    }

    public function filterHall(Request $request)
    {
        $trainers = Client::where(function ($query) use ($request) {
            $query->where('category', 'halls_sideboard');
            if ($request->has('city_id')) {
                $query->where('city_id', $request->city_id);
            }

            if ($request->has('specialization_id')) {
                $query->where('specialization_id', $request->city_id);
            }
        })->with('photos')->paginate(20);
        return responseJson(1, 'تم التحميل', $trainers);
    }
    /***********************************************************************************************/
    //Conferences Service
    public function filterConferences(Request $request)
    {
        $conferences = Client::where(function ($query) use ($request) {
            $query->where('category', 'conferences');
            if ($request->has('city_id')) {
                $query->where('city_id', $request->city_id);
            }
            if ($request->has('specialization_id')) {
                $query->where('specialization_id', $request->city_id);
            }
        })->with('photos')->paginate(20);
        return responseJson(1, 'تم التحميل', $conferences);
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\Photo;

class PhotoController extends Controller
{
    //
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        if(file_exists($photo))
        unlink($photo->url);
        $photo->delete();
        $data = [
            'status' => 1,
            'msg' => 'تم الحذف بنجاح',
            'id' => $photo->id
        ];
        return response()->json($data, 200);
    }
}

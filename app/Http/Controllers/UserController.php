<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Response;
use Hash;
use Auth;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changePassword()
    {
        return view('admin.reset-password');
    }

    public function changePasswordSave(Request $request)
    {
        $this->validate($request, [
            'old-password' => 'required',
            'password' => 'confirmed',
        ]);

        $user = Auth::user();

        if (Hash::check($request->input('old-password'), $user->password)) {
            // The passwords match...
            if ($request->has('email')) {
                $user->email = $request->email;
            }
            if ($request->has('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();
            flash()->success('  data has been changed successfully');
            return view('admin.reset-password');
        } else {
            flash()->error('   password is not correct ');
            return view('admin.reset-password');
        }
    }

    public function index()
    {
        $users = User::with('roles')->latest()->paginate(20);
        return view('admin.users.index', compact('users'));
    }

    public function create(User $model)
    {
        return view('admin.users.create', compact('model'));
    }

    public  function store(Request $request){
        $this->validate($request, [
            'name'             => 'required',
            'password'           => 'required|confirmed',
            'email'              => 'required|unique:users,email',
            'roles'          => 'required',
        ]);
        $request->merge(array('api_token' => str_random(60)));
        $request->merge(array('password' => bcrypt($request->password)));
        $user =  User::create($request->all());
        if($request->roles)
            $user->roles()->attach($request->roles);
        if($user){
            flash()->success('client has been created successfully');
            return redirect('admin/user');
        }
        else{
            flash()->error('Failed To create user , please try again ');
            return redirect('admin/user');
        }

    }

    public function edit($id)
    {
        $model = USer::findOrFail($id);
        return view('admin.users.edit', compact('model'));
    }

    public function update(Request $request , $id){

        $this->validate($request, [
            'name'               => 'required',
            'password'           => 'confirmed',
            'email'              => 'required',
            'roles'              => 'required',
        ]);
        $user = User::findorfail($id);
        $user->update($request->except('password'));
        if($request->has('password')) {
            $user->password = bcrypt($request->password);
            $user->save();
        }
        if($request->roles)
        {
            $user->roles()->detach();
            $user->roles()->attach($request->roles);
        }

            flash()->success('client has been updated successfully');
            return redirect('admin/user/'.$user->id .'/edit');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        $data = [
            'status' => 1,
            'msg' => 'Book has been deleted successfully',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Entry;
use Illuminate\Http\Request;

use App\Http\Requests;

use Response;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $clients = Client::where(function($q) use($request){
            if($request->name )
            {
                $q->where('name','LIKE','%'.$request->name.'%');
            }

            if($request->email)
            {
                $q->where('email',$request->email);
            }
            if($request->state)
            {
                $q->where('state',$request->state);
            }
        })->latest()->paginate(20);
        return view('admin.clients.index',compact('clients'));
    }

    public function create(Client $model)
    {
        return view('admin.clients.create',compact('model'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nationality'          => 'required|alpha_dash',
            'speciality'          => 'required|alpha_dash',
            'phone'          => 'required',
            'name'          => 'required|alpha_dash',
            'email'          => 'required|email|unique:clients,email',
            'password'      => 'required|confirmed',
        ]);
        $token = str_random(60);
        $request->merge(array('api_token' => $token));
        $request->merge(array('state' => 'approved'));
        $request->merge(array('password' => bcrypt($request->password)));
        Client::create($request->all());
        flash()->success('  Client has been added successfully');
        return redirect('admin/client');
    }

    public function show($id)
    {
        $client = Client::findorfail($id);
        $entries = Entry::PublishedEntries()->with('book','client')->where('client_id',$client->id)->get();

        return view('admin.clients.show',compact('client','entries'));
    }

    public function edit($id)
    {
        $model = Client::findOrFail($id);
        return view('admin.clients.edit',compact('model'));
    }

    public function update(Request $request , $id)
    {
        $this->validate($request, [
            'nationality'          => 'required|alpha_dash',
            'speciality'          => 'required|alpha_dash',
            'phone'          => 'required',
            'name'          => 'required|alpha_dash',
            'email'          => 'required|email',
            'password'      => 'confirmed',
        ]);
       $client  = Client::findOrFail($id);
        $client->update($request->except('password'));
        if($request->has('password'))
        {
            $client->password = bcrypt($request->password);
        }
        flash()->success('  Client has been updated successfully');
        return redirect('admin/client/'.$id.'/edit');

    }

    public function destroy($id)
    {
        $client = Client::findOrFail($id);

        $client->delete();
        $data = [
            'status' => 1,
            'msg' => '  client has been deleted successfully',
            'id' => $id
        ];
        return Response::json($data, 200);
    }

    public function approve($id)
    {
        $client = Client::findOrFail($id);
        $client->state = 'approved';
        $client->save();
        flash()->success(' client has been activated successfully');
        return back();
    }

    public function unApprove($id)
    {
        $client = Client::findOrFail($id);
        $client->state = 'non_approved';
        $client->save();
        flash()->success(' client has been deactivated successfully');
        return back();
    }
}

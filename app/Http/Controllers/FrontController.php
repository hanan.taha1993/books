<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\Settings;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Client;
use App\Models\Book;

use Auth;
use DB;

class FrontController extends Controller
{


    public function __construct()
    {
        $setting = Settings::find(1);
        session()->put('ar_site_name', $setting->ar_site_name);
        session()->put('en_site_name', $setting->en_site_name);
    }
    
    public function index()
    {
    	$sliders = Slider::latest()->take(4)->get();
    	return view('front.index',compact('sliders'));
    }
    public function postLogin(Request $request)

    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        if (auth()->guard('client')->attempt(['email' => $request->email, 'state' => 'approved', 'password' => $request->password])) {
            return redirect()->intended(route('client.entries'));
        }
        else{
                        session()->flash('error','Login failed');
                        return redirect('/');
        }
    }
    public function postRegister(Request $request){

        $this->validate($request, [
            'phone'             => 'required',
            'Nationality'             => 'required',
            'Speciality'             => 'required',
            'gender'             => 'required',
            'name'             => 'required',
            'password'           => 'required|confirmed',
            'email'              => 'required|unique:clients,email',
        ]);
        $Token = str_random(60);
        $code = str_random(6);
        $request->merge(array('api_token' => $Token));
        $request->merge(array('code' => $code));
        $request->merge(array('password' => bcrypt($request->password)));
        $request->merge(array('state' => 'non_approved'));
        $client = Client::create($request->all());
        if($client)
        {
                session()->flash('success','Register success , please wait for approve ');
        }
        else{
                        session()->flash('error','Register success , please wait for approve ');
        }
        return redirect('/');
    }
    
    public  function logout(){
        Auth::guard('client')->logout();
        return redirect('/');
    }

    public  function entries()
    {
            $entries = Entry::publishedRatedEntries()->with('book', 'client')->get();
            return view('front.after_log', compact('entries'));
    }
    public  function books(Request $request){

        $books = Book::where(function($q) use($request){
            if($request->name)
            {
                $q->where('ar_name','LIKE','%'.$request->name.'%');
                $q->orwhere('en_name','LIKE','%'.$request->name.'%');

            }
            if($request->category_id)
            {
                $q->where('category_id',$request->category_id);
            }
            })->where('state','approved')->latest()->paginate(20);
        return view('front.books',compact('books'));
    }
    public  function book($id){
        $book = Book::findorfail($id);
        $entry = auth()->guard('client')->user()->entries()->publishedEntries()->where('book_id',$book->id)->count();
        return view('front.book',compact('book','entry'));
    }
    public  function getEntry($id){
        $book = Book::findorfail($id);
        $entry = new Entry;
        return view('front.new_work',compact('book','entry'));
    }
    public  function addEntry(Request $request){
        $ent_count = Entry::publishedEntries()->where('client_id',Auth::guard('client')->id())->count();
          $this->validate($request, [
                'book_id'             => 'required',
                'criticism'           => 'required',
                'summary'             => 'required',
                'critisim_prectange'   => 'required'
            ]);
            
        if($request->submit == 'Submit')
            {
            if($ent_count < settings()->share_limit )
               {
                $request->merge(array('draft' => false));
                }
                else{
                flash()->error('Failed , you have reached entries limit');
                return redirect()->back();
                }
            }
           else{
               $request->merge(array('draft' => true));
            }   
         $request->merge(['client_id'=>Auth::guard('client')->id()]);
            if($request->entry_id)
            { 
           $book= Entry::findorfail($request->entry_id)->update($request->all());
            }
            else{
             $book =  Entry::create($request->all());
            }
            if($book){
                flash()->success('Entry  has been sent successfully');
                return redirect()->back();
            }
            else{
                flash()->error('Failed , Entr  has not  been sent successfully');
                return redirect()->back();
            }
        
        
    


    }
    public function suggestBook(Request $request){

        $this->validate($request, [
            'name'             => 'required',
            'author'           => 'required',
            'description'      => 'required',
        ]);
        $request->merge(array('state' => 'pending'));
       $book =  Book::create($request->all());
        if($book){
            flash()->success('Suggestion  has been sent successfully');
            return redirect()->back();
        }
        else{
            flash()->error('Failed , Suggestion  has not  been sent successfully');
            return redirect()->back();
        }

    }
    public function getSuggestBook(){
        return view('front.suggest_book');
    }
    public function oldWork(){
        $works = Entry::draftEntries()->with('book')->where('client_id',Auth::guard('client')->id())->get();
            return view('front.old_work',compact('works'));
    }


    public function myEntry(){
        $entries =  Entry::PublishedRatedEntries()
     			->join('rates','rates.entry_id','=','entries.id')	
     			->select('entries.*', DB::raw('avg(rate_value) as total '), DB::raw('count(*) as count'))
     			->groupBy('entries.client_id')
     			->orderBy('total','DESC')
     			->get();
     		
        return view('front.rank',compact('entries'));
    }

    public  function getDraft($id){

        $entry = Entry::findorfail($id);
        $book = $entry->book;
        return view('front.new_work',compact('book','entry'));
    }
    }

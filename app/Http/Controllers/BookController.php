<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Entry;
use Illuminate\Http\Request;
use Response;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $books = Book::where(function($q) use($request){
            if($request->name)
            {
                $q->where('name','LIKE','%'.$request->en_name.'%');
                $q->orwhere('name','LIKE','%'.$request->ar_name.'%');

            }
            if($request->state)
            {
                $q->where('state',$request->state);
            }
            if(auth()->user()->hasRole('judge'))
            {
            	$q->where('state','approved');
            }
            })->latest()->paginate(20);
        return view('admin.books.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Book $model)
    {
        return view('admin.books.create',compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'en_name'             => 'required',
            'ar_name'             => 'required',
            'thumbnail'         => 'required|mimes:jpeg,jpg,png',
            'file'              => 'required|mimes:doc,pdf,docx,zip',
            'category_id'       => 'required',
        ]);
        $request->merge(['state'=>'approved']);
        $book = Book::create($request->except('thumbnail','file'));
        if($request->hasFile('thumbnail'))
        {
            $image = $request->file('thumbnail');
            $destinationPath = public_path().'/uploads/books';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given
            $book->update(['thumbnail' => 'uploads/books/'.$name]);
        }
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $filedestinationPath = public_path().'/uploads/books/files';
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $file->move($filedestinationPath, $name); // uploading file to given
            $book->update(['file' => 'uploads/books/files/'.$name]);
        }
        flash()->success(' Book has been added successfully');
        return redirect('admin/book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with('client')->findorfail($id);

        $entries = Entry::PublishedEntries()->with('client','book')->where('book_id',$book->id)->get();
        return view('admin.books.show',compact('book','entries'));
    }


    public function edit($id)
    {
        $model = Book::findOrFail($id);
        return view('admin.books.edit',compact('model'));
    }

    public function update(Request $request , $id)
    {

        $this->validate($request, [
            'en_name'             => 'required',
            'ar_name'             => 'required',
            'thumbnail'         => 'mimes:jpeg,jpg,png',
            'file'              => 'mimes:doc,pdf,docx,zip',
                        'category_id'       => 'required',

        ]);
        $book = Book::findOrFail($id);
        $book->update($request->except('thumbnail','file'));
         if ($request->hasFile('thumbnail'))
        {
            if (file_exists($book->thumbnail))
            {
                unlink($book->thumbnail); //unlink old image
            }
            $image = $request->file('thumbnail');
            $destinationPath = public_path().'/uploads/books';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given
            $book->update(['thumbnail' => 'uploads/books/'.$name]);
        }

        if ($request->file)
        {
            if (file_exists($book->file))
            {
                unlink($book->file); 
            }
            $file = $request->file('file');
            $filedestinationPath = public_path().'/uploads/books/files';
            $extension = $file->getClientOriginalExtension(); 
            $name = time().''.rand(11111,99999).'.'.$extension; 
            $file->move($filedestinationPath, $name); 
            $book->update(['file' => 'uploads/books/files/'.$name]);
        }
        flash()->success('Book has been updated successfully');
        return redirect('admin/book/'.$id.'/edit');

    }

    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        if(count($book->entries)>0)
    {
        $data = [
            'status' => 0,
            'msg' => 'Book cannot be deleted , it has entries',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
        $book->delete();
        $data = [
            'status' => 1,
            'msg' => 'Book has been deleted successfully',
            'id' => $id
        ];
        return Response::json($data, 200);
    }


    public function approve($id)
    {
        $record = Book::findOrFail($id);
        $record->state = 'approved';
        $record->save();
        flash()->success(' book has been approved');
        return back();
    }

    public function unApprove($id)
    {
        $record = Book::findOrFail($id);
        $record->state = 'pending';
        $record->save();
        flash()->success(' Book approved has been canceled ');
        return back();
    }
}


<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Response;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::latest()->paginate(20);
        return view('admin.sliders.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Slider $model)
    {
        return view('admin.sliders.create',compact('model'));
            }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'en_title'             => 'required',
            'ar_title'             => 'required',
            'thumbnail'         => 'required|mimes:jpeg,jpg,png',
        ]);
        $slider = Slider::create($request->except('thumbnail'));
        $image = $request->file('thumbnail');
        $destinationPath = public_path().'/uploads/sliders';
        $extension = $image->getClientOriginalExtension(); // getting image extension
        $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
        $image->move($destinationPath, $name); // uploading file to given
        $slider->update(['thumbnail' => 'uploads/sliders/'.$name]);
        flash()->success(' slider has been added successfully');
        return redirect('admin/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $model = Slider::findOrFail($id);
        return view('admin.sliders.edit',compact('model'));
    }

    public function update(Request $request , $id)
    {
        $this->validate($request, [
            'en_title'             => 'required',
            'ar_title'             => 'required',
            'thumbnail'         => 'mimes:jpeg,jpg,png',
        ]);
        $slider = Slider::findOrFail($id);
        $slider->update($request->except('thumbnail'));
        if ($request->hasFile('thumbnail'))
        {
            if (file_exists($slider->thumbnail))
            {
                unlink($slider->thumbnail); //unlink old image
            }
            $image = $request->file('thumbnail');
            $destinationPath = public_path().'/uploads/sliders';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time().''.rand(11111,99999).'.'.$extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given
            $slider->update(['thumbnail' => 'uploads/sliders/'.$name]);
        }
        flash()->success('slider has been updated successfully');
        return redirect('admin/slider/'.$id.'/edit');

    }

    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        $data = [
            'status' => 1,
            'msg' => 'slider has been deleted successfully',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
}

<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Category;
use Illuminate\Http\Request;
use Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(20);
        // return $categories;
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $model)
    {
        return view('admin.category.create',compact('model'));
            }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ar_name'     => 'required',
            'en_name'     => 'required',
        ]);

        Category::create($request->except('thumbnail'));
       
        flash()->success('Category has been added successfully ');
        return redirect('admin/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $model = Category::findOrFail($id);
        return view('admin.category.edit',compact('model'));
    }

    public function update(Request $request , $id)
    {
         $this->validate($request, [
            'ar_name'     => 'required',
            'en_name'     => 'required',
        ]);
        $category = Category::findOrFail($id);
    
        $category->update($request->all());
        flash()->success('category has been updated successfully   .');
        return redirect('admin/category/');

    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if(count($category->books))
        {
             $data = [
            'status' => 0,
            'msg' => 'category cannot delete , there are book related on  ',
            'id' => $id
        ]; 
        }
        else{
             $category->delete();
            $data = [
            'status' => 1,
            'msg' => 'category deleted successfully  ',
            'id' => $id
        ]; 
        }
      
        return Response::json($data, 200);
    }
}

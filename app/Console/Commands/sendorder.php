<?php

namespace App\Console\Commands;

use App\Models\Driver;
use Illuminate\Console\Command;
use DB;

class sendorder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:sendnotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send notification for nearset driver';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = 4;
        $distance = 20;
        $orders = DB::table('orders')
            ->where('state','pending')->get();
        foreach ($orders as $order) {
            $driver = Driver::selectRaw("*, ( 3959 * acos( cos( radians($order->client_Latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($order->client_longitude) ) + sin( radians($order->client_Latitude) ) * sin( radians( latitude ) ) ) ) AS distance")
                ->havingRaw("distance < $distance")
                // our constraints
                ->withCount('applied')
                ->having('applied_count','<',$limit)
                ->doesntHave('notifications',function($query) use($order){
                    $query->where('order_id',$order->id);
                })
                ->orderBy('distance','ASC')
                ->first();

            dd($driver);

        }
    }
}

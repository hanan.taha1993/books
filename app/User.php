<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait; // add this trait to your user model

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function categories()
    {
        return $this->hasMany('App\Models\Category');
    }

     public function sliders()
    {
        return $this->hasMany('App\Models\Slider');
    }

      public function products()
    {
        return $this->hasMany('App\Models\product');
    }

     public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'role_user');
    }

}

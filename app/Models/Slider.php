<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model 
{

    protected $table = 'sliders';
    public $timestamps = true;
    protected $fillable = array('en_title','ar_title', 'en_description','ar_description','thumbnail');


    public static function boot()
    {
        parent::boot();
        self::deleting(function($model){
            if(file_exists($model->thumbnail))
                unlink($model->thumbnail);
        });
    }

}
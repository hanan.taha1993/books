<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model 
{

    protected $table = 'settings';
    public $timestamps = true;
    protected $fillable = array('ar_site_name', 'en_site_name','facebook', 'twitter', 'linked_in', 'ar_about_us','en_about_us', 'share_limit','rating_item_file','about_contest_file');

}
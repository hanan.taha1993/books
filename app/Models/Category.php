<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{

    protected $table = 'categories';
    public $timestamps = true;
    protected $fillable = array('ar_name', 'en_name');

    public function books()
    {
        return $this->hasMany('App\Models\Book');
    }

}
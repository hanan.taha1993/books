<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model 
{

    protected $table = 'books';
    public $timestamps = true;
    protected $fillable = array('en_name','ar_name', 'en_description','ar_description', 'thumbnail','state','author','file','category_id');

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function entries()
    {
        return $this->hasMany('App\Models\Entry');
    }

    public function rates()
    {
        return $this->hasMany('App\Models\Rate');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function($model){
            if(file_exists($model->thumbnail))
                unlink($model->thumbnail);
        });
    }
}
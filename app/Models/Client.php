<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{

    use Notifiable;
    protected $guard = 'client';
    protected $table = 'clients';
    public $timestamps = true;
    protected $fillable = array('name', 'speciality','nationality','phone','gender', 'email', 'password','state','api_token','code');
    
    protected  $hidden = ['api_token','code','password', 'remember_token'];
    public function books()
    {
        return $this->hasMany('App\Models\Book');
    }

    public function entries()
    {
        return $this->hasMany('App\Models\Entry');
    }

}
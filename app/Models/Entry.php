<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model 
{

    protected $table = 'entries';
    public $timestamps = true;
    protected $fillable = array('book_id', 'summary', 'criticism', 'draft', 'client_id','critisim_prectange');

    public function book()
    {
        return $this->belongsTo('App\Models\Book');
    }

    public function rates()
    {
        return $this->hasMany('App\Models\Rate');
    }

    public function scopePublishedRatedEntries($query)
    {
        return $query->whereHas('rates',function($q){
            $q->where('draft',0);
        });
    }

    public function scopeDraftEntries($query){
        return $query->where(function ($q){
            $q->where('draft',1);
        });
    }
    public function scopePublishedEntries($query){
        return $query->where(function ($q){
            $q->where('draft',0);
        });
    }
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

}
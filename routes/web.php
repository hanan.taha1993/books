<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/',function(){
//    return redirect()->to('/home');
//});
Route::get('new-password', 'Api\Client\AuthController@newPassword');
Route::post('password', 'Api\Client\AuthController@password');
Route::get('/logout', 'Auth\LoginController@logout');
// admin routes
Route::group(['prefix'=> 'admin','middleware'=>'auth'],function(){
    //user reset password
    Route::get('user/change-password','UserController@changePassword');
    Route::post('user/change-password','UserController@changePasswordSave');
    Route::resource('user','UserController');
    //Settings Routes
    Route::get('setting','SettingController@view');
    Route::post('setting','SettingController@update');
    Route::resource('contacts','ContactController',['only' => ['index','destroy','show']]);
    Route::resource('slider', 'SliderController');
        Route::resource('category', 'CategoryController');
    Route::resource('photo', 'PhotoController');
    Route::resource('book','BookController');
    Route::resource('client','ClientController');
    Route::resource('entry','EntryController');
    Route::get('book/{id}/approve', 'BookController@approve');
    Route::get('book/{id}/un-approve', 'BookController@unApprove');
    Route::get('client/{id}/approve', 'ClientController@approve');
    Route::get('client/{id}/un-approve', 'ClientController@unApprove');
});

Route::group(['middleware'=>'language'], function(){
    //Language Controller 
    Route::get('lang/{lang}', 'LanguageController@switchLang');

Route::get('/','FrontController@index');
Route::post('client_login','FrontController@postLogin')->name('client_login');
Route::post('client_register','FrontController@postRegister');
Route::post('client/logout','FrontController@logout');
Route::get('client/entries','FrontController@entries')->name('client.entries');

Route::group(['prefix'=> 'client','middleware'=>'auth:client'],function() {
    Route::get('books','FrontController@books');
    Route::get('book/{id}','FrontController@book');
    Route::get('add_entry/{id}','FrontController@getEntry');
    Route::get('edit_draft/{id}','FrontController@getDraft');
    Route::post('add_entry','FrontController@addEntry');
    Route::get('suggest_book','FrontController@getSuggestBook');
    Route::post('suggest_book','FrontController@suggestBook');
    Route::get('old_work','FrontController@oldWork');
    Route::get('my_entry','FrontController@myEntry');

});
});



<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => 'locale', 'prefix' => 'v1'], function () {
    Route::get('cities', 'Api\MainController@cities');
    Route::get('interests', 'Api\MainController@interests');
    Route::get('specialties', 'Api\MainController@specialties');
    Route::get('sliders', 'Api\MainController@sliders');
    Route::get('settings', 'Api\MainController@settings');
    Route::get('categories','Api\MainController@categories');
    Route::group(['prefix' => 'client'], function () {
        Route::post('register', 'Api\Client\AuthController@register');
        Route::get('verify', 'Api\Client\AuthController@verify');
        Route::post('login', 'Api\Client\AuthController@login');
        Route::post('reset-password', 'Api\Client\AuthController@reset');
        Route::get('show-client', 'Api\Client\ClientController@showClient');

        Route::group(['middleware' => 'auth:client'], function () {
            Route::post('profile', 'Api\Client\AuthController@profile');
            Route::post('contact-us', 'Api\Client\ClientController@contactUs');
            Route::get('notifications', 'Api\Client\ClientController@notifications');
            Route::get('my-favourites', 'Api\Client\ClientController@myFavourites');
            Route::post('add-to-favourite', 'Api\Client\ClientController@favorite');
            Route::post('un-favourite', 'Api\Client\ClientController@unfavourite');


            Route::group(['prefix' => 'member'], function () {
                Route::post('register-in-course', 'Api\Client\ClientController@registerInCourse');
            });

            Route::group(['prefix' => 'trainer'], function () {
                Route::post('add-course', 'Api\Client\ClientController@addCourseByTrainer');
                Route::get('courses', 'Api\Client\ClientController@getTrainerCourses');
                Route::post('delete-course', 'Api\Client\ClientController@deleteTrainerCourse');
                Route::get('filter','Api\Client\ClientController@filterTrainers');

            });

             Route::group(['prefix' => 'institute'], function () {
                Route::post('add-course', 'Api\Client\ClientController@addCourseByInstitute');
                Route::get('courses', 'Api\Client\ClientController@getInstituteCourses');
                Route::post('delete-courses', 'Api\Client\ClientController@deleteInstituteCourse');
                Route::get('show-course', 'Api\Client\ClientController@getInstituteCourse');
                Route::get('filter','Api\Client\ClientController@filterInstitute');

            });

              Route::group(['prefix' => 'hall-sideboard'], function () {
                Route::post('add-hall', 'Api\Client\ClientController@addHall');
                Route::get('halls', 'Api\Client\ClientController@getHalls');
                Route::post('delete-hall', 'Api\Client\ClientController@deleteHall');


            });

            Route::group(['prefix' => 'conferences'], function () {
                Route::get('filter','Api\Client\ClientController@filterConferences');

            });
        });
    });

});